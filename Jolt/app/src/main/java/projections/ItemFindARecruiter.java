package projections;

/**
 * Created by rishabh on 4/22/2017.
 */

public class ItemFindARecruiter {
    public String id;
    public String company;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getService_offered() {
        return service_offered;
    }

    public void setService_offered(String service_offered) {
        this.service_offered = service_offered;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String address;
    public String website;
    public String telephone;
    public String fax;
    public String service_offered;
}
