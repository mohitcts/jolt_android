package projections;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by rishabh on 3/31/2017.
 */

public class ItemCareerPlaning {
    public String name;
    public String excerpt;

    public JSONObject getRefs() {
        return refs;
    }

    public void setRefs(JSONObject refs) {
        this.refs = refs;
    }

    public JSONObject refs;

    public JSONArray getTags() {
        return tags;
    }

    public void setTags(JSONArray tags) {
        this.tags = tags;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONArray tags;
}
