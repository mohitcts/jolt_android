package projections;

/**
 * Created by rishabh on 4/1/2017.
 */

public class ItemCoachcategories {
    public String catid;

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String category_name;
}
