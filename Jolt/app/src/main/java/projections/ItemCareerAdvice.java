package projections;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by taran on 4/11/2017.
 */

public class ItemCareerAdvice {
    private String name;
    private String excerpt;

    public JSONObject refs;

    public JSONArray getLocations() {
        return locations;
    }

    public void setLocations(JSONArray locations) {
        this.locations = locations;
    }

    public JSONArray locations;

    public JSONObject getCompany() {
        return company;
    }

    public void setCompany(JSONObject company) {
        this.company = company;
    }

    public JSONObject company;

    public JSONArray getTags() {
        return tags;
    }

    public void setTags(JSONArray tags) {
        this.tags = tags;
    }

    public JSONObject getRefs() {
        return refs;
    }

    public void setRefs(JSONObject refs) {
        this.refs = refs;
    }

    public JSONArray tags;

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
