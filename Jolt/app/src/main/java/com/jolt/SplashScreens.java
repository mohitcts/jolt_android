package com.jolt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import jolt.registration.LoginActivity;
import question.activities.DecisiveActivity;
import question.activities.GraphActivity;
import utils.AppConstants;
import utils.Keys;

public class SplashScreens extends Activity {
    public static final int SPLASH_HOLD_TIME=2000;
    private Handler handler;
    private Runnable runnable;
    private SharedPreferences sharedpreferences;
    private String user_id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screens);

      /*  int secondDelay=1;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                finish();
            }
        },secondDelay *SPLASH_HOLD_TIME);
*/
    }

    private void getUserData() {
        sharedpreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Keys.ID)) {
            user_id = sharedpreferences.getString(Keys.ID, "");
        }

    }



    @Override
    protected void onResume() {
        super.onResume();

        getUserData();
        if(user_id.equals("")) {
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashScreens.this, LoginActivity.class));
                    finish();
                }
            };
            handler.postDelayed(runnable, 3000);
        }else {
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashScreens.this, MainActivity.class));
                    finish();
                }
            };
            handler.postDelayed(runnable, 3000);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);

    }
}
