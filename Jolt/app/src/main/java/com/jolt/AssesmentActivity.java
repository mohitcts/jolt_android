package com.jolt;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import question.activities.DecisiveActivity;

public class AssesmentActivity extends AppCompatActivity implements View.OnClickListener {

    Button buttonDecisive;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assesment);
        setUpToolbar();
        initFields();
    }


    private void initFields(){
        buttonDecisive=(Button)findViewById(R.id.buttonDecisive);
        buttonDecisive.setOnClickListener(this);
    }

    // setting up toolbar
    private void setUpToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.assessments));
        if (toolbar == null) return;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.leftarrow);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonDecisive:
                Intent i = new Intent(this,DecisiveActivity.class);
                startActivity(i);
                break;
        }
    }
}
