package com.jolt;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import question.activities.BusinessCheckList;
import question.activities.CareerPlannerActivity;
import question.activities.CoachTypeActivity;
import question.activities.CurrentEmployeeCheckList;
import question.activities.EmploymentTransactionCheckList;
import question.activities.FeedbackActivity;
import question.activities.InstructionActivity;
import question.activities.JobBoardActivity;
import question.activities.LeaderCheckListActivity;
import question.activities.MiniBusinessPlanActivity;
import question.activities.NewEmployeeCheckListActivity;
import question.activities.ProfileActivity;
import question.activities.ResourcesActivity;
import question.activities.StoreActivity;
import utils.AppConstants;
import utils.Keys;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imageViewCoaches,jobBoardHome,PlanningToolsHome,assessmentTestsHome;
    BottomNavigationView bottomNavigationView;
    private TextView textViewUserName,textViewFeedback;
    private SharedPreferences sharedpreferences;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            if (getIntent().getExtras() != null) {
                Bundle bundle;
                bundle = getIntent().getExtras();
                String notificaton_type = bundle.getString("notification_type").replaceAll("^\"|\"$", "");
                Log.e("notificaton_type", notificaton_type);
                if (notificaton_type.equals("Mini Business Plan")) {
                    Intent intent = new Intent(MainActivity.this, MiniBusinessPlanActivity.class);
                    startActivity(intent);
                }else if (notificaton_type.equals("Career Plan")) {
                    Intent intent = new Intent(MainActivity.this, CareerPlannerActivity.class);
                    startActivity(intent);
                }else if (notificaton_type.equals("Business Checklist")) {
                    Intent intent = new Intent(MainActivity.this, BusinessCheckList.class);
                    //intent.putExtras(bundle);
                    startActivity(intent);
                }else if (notificaton_type.equals("Business Checklist Quarter")) {
                    showAlertDialog();
                }else if (notificaton_type.equals("New Employee")) {
                    Intent intent = new Intent(MainActivity.this, NewEmployeeCheckListActivity.class);
                    //intent.putExtras(bundle);
                    startActivity(intent);
                }else if (notificaton_type.equals("Current Employee")) {
                    Intent intent = new Intent(MainActivity.this, CurrentEmployeeCheckList.class);
                    //intent.putExtras(bundle);
                    startActivity(intent);
                }else if (notificaton_type.equals("Leader")) {
                    Intent intent = new Intent(MainActivity.this, LeaderCheckListActivity.class);
                    //intent.putExtras(bundle);
                    startActivity(intent);
                }else if (notificaton_type.equals("Employment")) {
                    Intent intent = new Intent(MainActivity.this, EmploymentTransactionCheckList.class);
                    //intent.putExtras(bundle);
                    startActivity(intent);

                }
                //getUserData();
                return;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        getUserData();
        setupToolbar();
        initFields();
        initListeners();
    }

    private void initListeners() {
        textViewFeedback.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppConstants.isMainActivityPageOpen = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppConstants.isMainActivityPageOpen = false;
    }

    private void getUserData() {
        sharedpreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Keys.NAME)) {
            name = sharedpreferences.getString(Keys.NAME, "");
        }
    }


    private void initFields(){
        imageViewCoaches=(ImageView)findViewById(R.id.imageViewCoaches);
        jobBoardHome=(ImageView)findViewById(R.id.jobBoardHome);
        PlanningToolsHome=(ImageView)findViewById(R.id.PlanningToolsHome);
        assessmentTestsHome=(ImageView)findViewById(R.id.assessmentTestsHome);
        textViewUserName=(TextView) findViewById(R.id.textViewUserName);
        textViewFeedback=(TextView) findViewById(R.id.textViewFeedback);
        textViewUserName.setText(name);
        PlanningToolsHome.setOnClickListener(this);
        imageViewCoaches.setOnClickListener(this);
        jobBoardHome.setOnClickListener(this);
        PlanningToolsHome.setOnClickListener(this);
        assessmentTestsHome.setOnClickListener(this);

         bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){
                    case R.id.action_mybooks:
                       // divert();
                        storeActivityCall();

                        break;
                    case R.id.action_dashboard:
                        break;

                    case R.id.action_profile:
                        profileActivityCall();
                        break;
                }
                return true;
            }
        });
    }
    private void divert() {
        Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://35.167.21.236/transcendproserv/product-category/publications/"));
        startActivity(viewIntent);
    }


    private void profileActivityCall(){
        startActivity(new Intent(this, ProfileActivity.class));

    }

    private void storeActivityCall(){
        startActivity(new Intent(this,StoreActivity.class));
    }


    // setting all the functionality of toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.dashboardHeader));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
       getSupportActionBar().setDisplayShowTitleEnabled(false);
      //  ab.setHomeAsUpIndicator(R.mipmap.ic_close_black_24dp);
       // ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textViewFeedback:
                startActivity(new Intent(MainActivity.this, FeedbackActivity.class));
                break;
            case R.id.imageViewCoaches:
                coachActivityCall();
                break;
            case R.id.PlanningToolsHome:
                Bundle bundle=new Bundle();
                bundle.putString("value","0");
                Intent i = new Intent(this, InstructionActivity.class);
                i.putExtras(bundle);
                startActivity(i);
                break;
            case R.id.jobBoardHome:
                Intent intentt = new Intent(this, JobBoardActivity.class);
                startActivity(intentt);
                break;
            case R.id.assessmentTestsHome:
                bundle=new Bundle();
                bundle.putString("value","1");
                Intent intent = new Intent(this, InstructionActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }

    private void coachActivityCall(){
        startActivity(new Intent(this, CoachTypeActivity.class));
      //  finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppConstants.isMainActivityPageOpen = true;
        if (AppConstants.isFromNotification) {
            AppConstants.isFromNotification = false;
            clearNotifications(getApplicationContext());
            getNotificationDetails();
        }

    }

    private void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    private void getNotificationDetails() {
        if (AppConstants.isBusinessChecklist) {
            AppConstants.isBusinessChecklist = false;
            //AppConstants.isNotification = true;
            startActivity(new Intent(MainActivity.this, BusinessCheckList.class));
            return;
        }
        if (AppConstants.isBusinessChecklistQuarter) {
            AppConstants.isBusinessChecklistQuarter = false;
            //AppConstants.isNotification = true;
            showAlertDialog();
            return;
        }
        if (AppConstants.isNewEmployee) {
            AppConstants.isNewEmployee = false;
            //AppConstants.isNotification = true;
            startActivity(new Intent(MainActivity.this, NewEmployeeCheckListActivity.class));
            return;
        }
        if (AppConstants.isMinibusinessPlan) {
            AppConstants.isMinibusinessPlan = false;
            //AppConstants.isNotification = true;
            startActivity(new Intent(MainActivity.this, MiniBusinessPlanActivity.class));
            return;
        }
        if (AppConstants.isCareerPlan) {
            AppConstants.isCareerPlan = false;
            //AppConstants.isNotification = true;
            startActivity(new Intent(MainActivity.this, NewEmployeeCheckListActivity.class));
            return;
        }
        if (AppConstants.isEmployee) {
            AppConstants.isEmployee = false;
            //AppConstants.isNotification = true;
            startActivity(new Intent(MainActivity.this, CurrentEmployeeCheckList.class));
            return;
        }
        if (AppConstants.isLeader) {
            AppConstants.isLeader = false;
            //AppConstants.isNotification = true;
            startActivity(new Intent(MainActivity.this, LeaderCheckListActivity.class));
            return;
        }
        if (AppConstants.isEmployment) {
            AppConstants.isEmployment = false;
            //AppConstants.isNotification = true;
            startActivity(new Intent(MainActivity.this, EmploymentTransactionCheckList.class));
            return;
        }
    }

    private void showAlertDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Are you on track or behind your goal?");
        builder.setPositiveButton("Behind", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //getLogout();
                moveToNextActivity();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();
    }

    private void moveToNextActivity() {
        Intent intent=new Intent(MainActivity.this, ResourcesActivity.class);
        startActivity(intent);
    }
}
