package question.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.BusinessCheckListAdapter;
import projections.ItemBussinessChecklist;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.DataParser;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.WebServiceCall;

public class EmploymentTransactionCheckList extends AppCompatActivity implements View.OnClickListener{

    private ConnectionDetector connectionDetector;
    private ArrayList<Integer> checkboxListStatus;
    private ArrayList<String> checkedArrayListInitial;
    private SharedPreferences sharedpreferences;
    private String user_id;
    private Button buttonNext;
    private ListView listViewBussinessCheckList;
    private ArrayList<String> checkedArraylist;
    private ArrayList<ItemBussinessChecklist> bussinessCheckListArrayList;
    private BusinessCheckListAdapter businessCheckListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_employee_check_list);
        connectionDetector=new ConnectionDetector(EmploymentTransactionCheckList.this);
        checkboxListStatus=new ArrayList<>();
        checkedArrayListInitial=new ArrayList<>();

        initFieldListner();
        setupToolbar();
        getUserData();
        getAllList();

    }
    private void getAllList() {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("user_id",user_id);
            jsonObject.put("type","Employment");
        } catch (JSONException e) {

        }
        getCheckListData(jsonObject);
    }


    private void getUserData() {
        sharedpreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Keys.ID)) {
            user_id = sharedpreferences.getString(Keys.ID, "");
        }
    }
    // initilization value
    private void initFieldListner() {
        buttonNext = (Button) findViewById(R.id.buttonNext);
        listViewBussinessCheckList = (ListView) findViewById(R.id.listViewBussinessCheckList);
        buttonNext.setOnClickListener(this);
    }

    // setting up toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.employment_transaction));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }


    @Override
    public void onClick(View v) {
        if(v==buttonNext){
            checkedArraylist = businessCheckListAdapter.getCheckedArrayList();

            JSONObject jsonObjectCheckListID=new JSONObject();
            for(int i= 0;i<checkedArraylist.size();i++){
                try {
                    jsonObjectCheckListID.put(i+"",checkedArraylist.get(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            JSONObject jsonObject=new JSONObject();
            try {
                jsonObject.put("user_id",user_id);
                jsonObject.put("type","Employment");
                jsonObject.put("checklist_id",jsonObjectCheckListID);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            submitCheckListData(jsonObject);
        }
    }

    private void getCheckListData(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(EmploymentTransactionCheckList.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            WebServiceCall webServiceCall = new WebServiceCall(EmploymentTransactionCheckList.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        if (!response.equals("")) {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean success = jsonObject.getBoolean("success");
                            if (success) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                DataParser dataParser = new DataParser();
                                bussinessCheckListArrayList = dataParser.parseBussinessCheckListData(jsonArray);

                                for (int i=0;i<bussinessCheckListArrayList.size();i++){
                                    String check_status=bussinessCheckListArrayList.get(i).getCheck_status();
                                    if(check_status.equals("1")){
                                        checkboxListStatus.add(i);
                                        checkedArrayListInitial.add(bussinessCheckListArrayList.get(i).getQuestion_id());
                                    }
                                }

                                businessCheckListAdapter = new BusinessCheckListAdapter(EmploymentTransactionCheckList.this, bussinessCheckListArrayList,checkboxListStatus,checkedArrayListInitial);
                                listViewBussinessCheckList.setAdapter(businessCheckListAdapter);

                            }

                        } else {
                            Toast.makeText(EmploymentTransactionCheckList.this, "Something went wrong.Try Again.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onError(String error) {

                    progressDialog.dismiss();
                    Toast.makeText(EmploymentTransactionCheckList.this, error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.ALL_CHECKLIST_URL);
        } else {
            Toast.makeText(EmploymentTransactionCheckList.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void submitCheckListData(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(EmploymentTransactionCheckList.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(EmploymentTransactionCheckList.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        String message = jsonObjectResponse.getString("message");
                        if (success) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.ADD_CHECKLIST_URL);
            progressDialog.show();
        } else {
            Toast.makeText(EmploymentTransactionCheckList.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }
}