package question.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.MainActivity;
import com.jolt.R;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import jolt.registration.LoginActivity;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.WebServiceCall;

public class StoreActivity extends AppCompatActivity implements View.OnClickListener {


    private Button bookBtn, detailedBusinessPlanBtn, coreValuePlanBtn, investorsPitchBtn;
    private String pdf, plan_amount = "",detailed_payment_status="",core_payment_status="",investors_payment_status="";
    private int REQUEST_CODE_PAYMENT = 1;

    private static final String CONFIG_CLIENT_ID = "Achb2TS0qpvaRGaWawF0fa3urt0LlkfVoHpTa21QqZbL--PEaFgK10qsFaj86jppvdWPdZGT1YCPqAOV";
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            /*.merchantName("LeService")*/
            .merchantName("Jolt")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
    private String payment_name;
    private ConnectionDetector connectionDetector;
    private String name, user_id;
    private SharedPreferences sharedpreferences;
    private JSONObject jsonObjectCheckPlans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        connectionDetector = new ConnectionDetector(StoreActivity.this);

        setupToolbar();
        initialization();

        getUserData();
        getStatus();
    }

    private void getStatus() {

        jsonObjectCheckPlans = new JSONObject();
        try {
            jsonObjectCheckPlans.put("user_id", user_id);

            JSONObject jsonObjectPlans = new JSONObject();
            jsonObjectPlans.put("0", "Detailed Business Plan");
            jsonObjectPlans.put("1", "Core Value Statement");
            jsonObjectPlans.put("2", "Investors Pitch");

            jsonObjectCheckPlans.put("plan_names", jsonObjectPlans);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        getPaymentDetails(jsonObjectCheckPlans);
    }

    private void getUserData() {
        sharedpreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Keys.ID)) {
            user_id = sharedpreferences.getString(Keys.ID, "");
        }
    }

    private void initialization() {
        bookBtn = (Button) findViewById(R.id.bookBtn);
        detailedBusinessPlanBtn = (Button) findViewById(R.id.detailedBusinessPlanBtn);
        coreValuePlanBtn = (Button) findViewById(R.id.coreValuePlanBtn);
        investorsPitchBtn = (Button) findViewById(R.id.investorsPitchBtn);
        bookBtn.setOnClickListener(this);
        detailedBusinessPlanBtn.setOnClickListener(this);
        coreValuePlanBtn.setOnClickListener(this);
        investorsPitchBtn.setOnClickListener(this);

    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.STORE));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bookBtn:
                divert();
                break;
            case R.id.detailedBusinessPlanBtn:
                plan_amount = "5";
                payment_name = "Detailed Business Plan";
                pdf = "http://35.167.21.236/joltapi/assets/pdf/Detailed%20Business%20Plan.pdf";

                if(detailed_payment_status.equals("")){
                    getPaymentDetails(jsonObjectCheckPlans);
                    Toast.makeText(StoreActivity.this, "Try Again.", Toast.LENGTH_SHORT).show();
                }else if(detailed_payment_status.equals("0")){
                    onBuyPressed(plan_amount);
                }else if(detailed_payment_status.equals("1")){
                    Bundle bundle = new Bundle();
                    bundle.putString("pdf", pdf);
                    bundle.putString("value", payment_name);
                    bundle.putString("from", "");
                    Intent intent = new Intent(StoreActivity.this, PaidAssessmentActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
            case R.id.coreValuePlanBtn:
                plan_amount = "5";
                payment_name = "Core Value Statement";
                pdf = "http://35.167.21.236/joltapi/assets/pdf/Core%20Value%20Statement.pdf";

                if(core_payment_status.equals("")){
                    getPaymentDetails(jsonObjectCheckPlans);
                    Toast.makeText(StoreActivity.this, "Try Again.", Toast.LENGTH_SHORT).show();
                }else if(core_payment_status.equals("0")){
                    onBuyPressed(plan_amount);
                }else if(core_payment_status.equals("1")){
                    Bundle bundle = new Bundle();
                    bundle.putString("pdf", pdf);
                    bundle.putString("value", payment_name);
                    bundle.putString("from", "");
                    Intent intent = new Intent(StoreActivity.this, PaidAssessmentActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
            case R.id.investorsPitchBtn:
                plan_amount = "5";
                payment_name = "Investors Pitch";
                pdf = "http://35.167.21.236/joltapi/assets/pdf/Investors%20pitch%20development.pdf";

                if(investors_payment_status.equals("")){
                    getPaymentDetails(jsonObjectCheckPlans);
                    Toast.makeText(StoreActivity.this, "Try Again.", Toast.LENGTH_SHORT).show();
                }else if(investors_payment_status.equals("0")){
                    onBuyPressed(plan_amount);
                }else if(investors_payment_status.equals("1")){
                    Bundle bundle = new Bundle();
                    bundle.putString("pdf", pdf);
                    bundle.putString("value", payment_name);
                    bundle.putString("from", "");
                    Intent intent = new Intent(StoreActivity.this, PaidAssessmentActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
        }
    }


    private void divert() {
        Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://192.225.175.100/dev/transcendproserv/product-category/publications/"));
        startActivity(viewIntent);
    }

    private void toast() {
        Toast.makeText(this, "yet to implemented", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }

    private void getPaymentDetails(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            /*final ProgressDialog progressDialog = new ProgressDialog(StoreActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);*/
            WebServiceCall webServiceCall = new WebServiceCall(StoreActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        Log.d(this.getClass().getName(), "LOGIN_RESPONSE" + response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        String message = jsonObjectResponse.getString("message");
                        if (success) {
                            JSONArray jsonArray = jsonObjectResponse.getJSONArray("data");
                            JSONObject jsonObject1 = new JSONObject();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                detailed_payment_status = jsonArray.getJSONObject(0).getString("payment_status");
                                core_payment_status = jsonArray.getJSONObject(1).getString("payment_status");
                                investors_payment_status = jsonArray.getJSONObject(2).getString("payment_status");

                                if(detailed_payment_status.equals("1")){
                                    detailedBusinessPlanBtn.setText("Detailed Business Plan");
                                }
                                if(core_payment_status.equals("1")){
                                    coreValuePlanBtn.setText("Core Value Statement");
                                }
                                if(investors_payment_status.equals("1")){
                                    investorsPitchBtn.setText("Investors Pitch");
                                }
                            }
                        } else {
                            Toast.makeText(StoreActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                       // progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    //progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.GET_PAYMENT_DETAILS);
            //progressDialog.show();
        } else {
            Toast.makeText(StoreActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    public void onBuyPressed(String amount) {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE, amount);
        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */
        Intent intent = new Intent(StoreActivity.this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent, String amount) {
        return new PayPalPayment(new BigDecimal(amount), "USD", payment_name,
                paymentIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                      /*  String str=confirm.toJSONObject().getJSONObject("proof_of_payment").getJSONObject("adaptiv‌​e_payment").getString("pay_key");
                        Log.i(TAG, str);*/
                        String stringConfirm = confirm.toJSONObject().toString();
                        JSONObject jsonObjectConfirm = new JSONObject(stringConfirm);
                        JSONObject jsonObjectResponse = jsonObjectConfirm.getJSONObject("response");
                        String payment_id = jsonObjectResponse.getString("id");
                        String payment_date = jsonObjectResponse.getString("create_time");
                        String status_payment = jsonObjectResponse.getString("state");

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("transaction_id", payment_id);
                        jsonObject.put("payment_date", payment_date);
                        jsonObject.put("payment_status", status_payment);
                        jsonObject.put("user_id", user_id);
                        jsonObject.put("amount", plan_amount);
                        jsonObject.put("plan_name", payment_name);
                        // jsonObject.put("amount",amount);

                        Log.i("Paypal", payment_id);
                        Log.i("Paypal", payment_date);
                        // addOrder(paidAmount, FROM_PAYPAL);
                        //  saveLastTransactionDetails(payment_id,payment_date);
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        Toast.makeText(
                                StoreActivity.this,
                                "PaymentConfirmation info received from PayPal", Toast.LENGTH_LONG)
                                .show();

                        submitPaymentDetails(jsonObject);

                    } catch (JSONException e) {
                        Log.e("Paypal", "an extremely unlikely failure occurred: ", e);
                    } finally {
                        //   buttonAddMoney.setEnabled(true);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("Paypal", "The user canceled.");
                //  buttonAddMoney.setEnabled(true);
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "Paypal",
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
                //   buttonAddMoney.setEnabled(true);
            }
        }
    }

    private void submitPaymentDetails(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(StoreActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(StoreActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        Log.d(this.getClass().getName(), "LOGIN_RESPONSE" + response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        String message = jsonObjectResponse.getString("message");
                        if (success) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                            //pdf = "http://35.167.21.236/joltapi/assets/pdf/Detailed%20Business%20Plan.pdf";
                            Bundle bundle = new Bundle();
                            bundle.putString("pdf", pdf);
                            bundle.putString("value", payment_name);
                            bundle.putString("from", "");
                            Intent intent = new Intent(StoreActivity.this, PaidAssessmentActivity.class);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            // moveToNextActivity();
                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.SUBMIT_PAYMENT_DETAILS);
            progressDialog.show();
        } else {
            Toast.makeText(StoreActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }
}
