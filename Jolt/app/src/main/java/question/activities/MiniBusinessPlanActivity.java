package question.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;
import com.squareup.picasso.Picasso;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import jolt.registration.LoginActivity;
import permissions.MarshmallowPermission;
import permissions.PermissionKeys;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.Downloader;
import utils.INetworkResponse;
import utils.Keys;
import utils.MultipartWebServiceCall;
import utils.ServiceUrls;
import utils.WebServiceCall;

public class MiniBusinessPlanActivity extends AppCompatActivity implements View.OnClickListener {
    private Button buttonNext,buttonCompanyLogo;
    private EditText editTextDate,editTextCompanyName,editTextCompanyMotto,editTextMissionStatement,editTextMarketAnalysis,
            editTextCareerObjective1,editTextCareerObjective2,editTextCareerObjective3,editTextCareerObjective4,editTextCareerObjective5,
            editTextCareerGoals1,editTextCareerGoals2,editTextCareerGoals3,editTextCareerGoals4,editTextCareerGoals5,
            editTextActionPlan1,editTextActionPlan2,editTextActionPlan3,editTextActionPlan4,editTextActionPlan5,
            editTextBussinessRequirement1,editTextBussinessRequirement2,editTextBussinessRequirement3,editTextBussinessRequirement4,editTextBussinessRequirement5;
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private String date_set_start;
    private MarshmallowPermission marshmallowPermission;
    private Uri imgUri;
    private static final int CAMERA_REQUEST = 202;
    private String imagePath,pdfurl,id,date,company_mane,user_id,company_motto,market_analysis,mission;
    private SharedPreferences sharedpreferences;
    private ConnectionDetector connectionDetector;
    private MultipartEntityBuilder multipartEntityBuilder;
    private FileBody file;
    private File fileURI;
    private Uri uriDocument;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_plan);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        marshmallowPermission=new MarshmallowPermission(MiniBusinessPlanActivity.this);
        connectionDetector=new ConnectionDetector(MiniBusinessPlanActivity.this);

        // calling toolbar
        setupToolbar();

        initializer();
        initFieldListner();
        getUserData();
    }

    private void getUserData() {
        sharedpreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Keys.ID)) {
            user_id = sharedpreferences.getString(Keys.ID, "");
        }
    }

    private void initializer() {
        buttonNext = (Button) findViewById(R.id.buttonNext);
        buttonCompanyLogo = (Button) findViewById(R.id.buttonCompanyLogo);

        editTextDate=(EditText)findViewById(R.id.editTextDate);
        editTextCompanyName=(EditText)findViewById(R.id.editTextCompanyName);
        editTextCompanyMotto=(EditText)findViewById(R.id.editTextCompanyMotto);
        editTextMissionStatement=(EditText)findViewById(R.id.editTextMissionStatement);
        editTextMarketAnalysis=(EditText)findViewById(R.id.editTextMarketAnalysis);

        editTextCareerObjective1=(EditText)findViewById(R.id.editTextCareerObjective1);
        editTextCareerObjective2=(EditText)findViewById(R.id.editTextCareerObjective2);
        editTextCareerObjective3=(EditText)findViewById(R.id.editTextCareerObjective3);
        editTextCareerObjective4=(EditText)findViewById(R.id.editTextCareerObjective4);
        editTextCareerObjective5=(EditText)findViewById(R.id.editTextCareerObjective5);

        editTextCareerGoals1=(EditText)findViewById(R.id.editTextCareerGoals1);
        editTextCareerGoals2=(EditText)findViewById(R.id.editTextCareerGoals2);
        editTextCareerGoals3=(EditText)findViewById(R.id.editTextCareerGoals3);
        editTextCareerGoals4=(EditText)findViewById(R.id.editTextCareerGoals4);
        editTextCareerGoals5=(EditText)findViewById(R.id.editTextCareerGoals5);

        editTextActionPlan1=(EditText)findViewById(R.id.editTextActionPlan1);
        editTextActionPlan2=(EditText)findViewById(R.id.editTextActionPlan2);
        editTextActionPlan3=(EditText)findViewById(R.id.editTextActionPlan3);
        editTextActionPlan4=(EditText)findViewById(R.id.editTextActionPlan4);
        editTextActionPlan5=(EditText)findViewById(R.id.editTextActionPlan5);

        editTextBussinessRequirement1=(EditText)findViewById(R.id.editTextBussinessRequirement1);
        editTextBussinessRequirement2=(EditText)findViewById(R.id.editTextBussinessRequirement2);
        editTextBussinessRequirement3=(EditText)findViewById(R.id.editTextBussinessRequirement3);
        editTextBussinessRequirement4=(EditText)findViewById(R.id.editTextBussinessRequirement4);
        editTextBussinessRequirement5=(EditText)findViewById(R.id.editTextBussinessRequirement5);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            //mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                try {
                    String currentDate = dateFormatter.format(newDate.getTime());

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date tempDate = simpleDateFormat.parse(currentDate);
                    date_set_start = simpleDateFormat.format(tempDate);
                    fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    editTextDate.setText(date_set_start);
                } catch (ParseException ex) {
                    System.out.println("Parse Exception");
                }

                // editstartdate.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    // initlization views
    private void initFieldListner() {
        buttonNext.setOnClickListener(this);
        buttonCompanyLogo.setOnClickListener(this);

        editTextDate.setFocusable(false);
        editTextDate.setOnClickListener(this);
    }


    // setting up toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.mini_business_plan));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v == buttonNext) {
            date=editTextDate.getText().toString();
            company_mane=editTextCompanyName.getText().toString();
            company_motto=editTextCompanyMotto.getText().toString();
            mission=editTextMissionStatement.getText().toString();
            market_analysis=editTextMarketAnalysis.getText().toString();

            if(date.length()<=0){
                editTextDate.requestFocus();
                editTextDate.setHintTextColor(Color.RED);
                Toast.makeText(MiniBusinessPlanActivity.this, "Please Enter Date", Toast.LENGTH_SHORT).show();
            }else if(company_mane.length()<=0){
                editTextCompanyName.requestFocus();
                editTextCompanyName.setHintTextColor(Color.RED);
                Toast.makeText(MiniBusinessPlanActivity.this, "Please Enter Company Name", Toast.LENGTH_SHORT).show();
            }else if(company_motto.length()<=0){
                editTextCompanyMotto.requestFocus();
                editTextCompanyMotto.setHintTextColor(Color.RED);
                Toast.makeText(MiniBusinessPlanActivity.this, "Please Enter Company Motto", Toast.LENGTH_SHORT).show();
            }else if(mission.length()<=0){
                editTextMissionStatement.requestFocus();
                editTextMissionStatement.setHintTextColor(Color.RED);
                Toast.makeText(MiniBusinessPlanActivity.this, "Please Enter Mission Statement", Toast.LENGTH_SHORT).show();
            }else if(market_analysis.length()<=0){
                editTextMarketAnalysis.requestFocus();
                editTextMarketAnalysis.setHintTextColor(Color.RED);
                Toast.makeText(MiniBusinessPlanActivity.this, "Please Enter Market Analysis", Toast.LENGTH_SHORT).show();
            }else {

                String[] permissions = {PermissionKeys.PERMISSION_WRITE_EXTERNAL_STORAGE, PermissionKeys.PERMISSION_READ_EXTERNAL_STORAGE};
                if (marshmallowPermission.isPermissionGrantedAll(PermissionKeys.REQUEST_CODE_PERMISSION_ALL, permissions)) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("user_id", user_id);
                        jsonObject.put("plan_date", date);
                        jsonObject.put("company_name", company_mane);
                        jsonObject.put("company_moto", company_motto);
                        jsonObject.put("mission_statement", mission);
                        jsonObject.put("market_analysis", market_analysis);

                        jsonObject.put("objective1", editTextCareerObjective1.getText().toString());
                        jsonObject.put("objective2", editTextCareerObjective2.getText().toString());
                        jsonObject.put("objective3", editTextCareerObjective3.getText().toString());
                        jsonObject.put("objective4", editTextCareerObjective4.getText().toString());
                        jsonObject.put("objective5", editTextCareerObjective5.getText().toString());

                        jsonObject.put("goals1", editTextCareerGoals1.getText().toString());
                        jsonObject.put("goals2", editTextCareerGoals2.getText().toString());
                        jsonObject.put("goals3", editTextCareerGoals3.getText().toString());
                        jsonObject.put("goals4", editTextCareerGoals4.getText().toString());
                        jsonObject.put("goals5", editTextCareerGoals5.getText().toString());

                        jsonObject.put("action_plan1", editTextActionPlan1.getText().toString());
                        jsonObject.put("action_plan2", editTextActionPlan2.getText().toString());
                        jsonObject.put("action_plan3", editTextActionPlan3.getText().toString());
                        jsonObject.put("action_plan4", editTextActionPlan4.getText().toString());
                        jsonObject.put("action_plan5", editTextActionPlan5.getText().toString());

                        jsonObject.put("requirement_plan1", editTextBussinessRequirement1.getText().toString());
                        jsonObject.put("requirement_plan2", editTextBussinessRequirement2.getText().toString());
                        jsonObject.put("requirement_plan3", editTextBussinessRequirement3.getText().toString());
                        jsonObject.put("requirement_plan4", editTextBussinessRequirement4.getText().toString());
                        jsonObject.put("requirement_plan5", editTextBussinessRequirement5.getText().toString());

                        multipartEntityBuilder = MultipartEntityBuilder.create();
                        multipartEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                        if (imagePath != null) {
                            file = new FileBody(new File(imagePath));
                            multipartEntityBuilder.addPart("logo", file);
                        }
                        multipartEntityBuilder.addPart(Keys.DATA, new StringBody(jsonObject.toString(), ContentType.TEXT_PLAIN));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    sendUserRequestData(multipartEntityBuilder);
                }
            }
        }
        if(v==editTextDate){
            fromDatePickerDialog.show();
            fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        }
        if(v==buttonCompanyLogo){
            String[] permissions = {PermissionKeys.PERMISSION_CAMERA, PermissionKeys.PERMISSION_READ_EXTERNAL_STORAGE};
            if (marshmallowPermission.isPermissionGrantedAll(PermissionKeys.REQUEST_CODE_PERMISSION_ALL, permissions)) {
                selectImage();
            }
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Profile Image!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    captureImage();
                }
                if (which == 1) {
                    openGallery();
                }
            }
        });
        builder.show();
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoPickerIntent, 5);
    }

    /*private void captureImage() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        (new File("/sdcard/jolt/")).mkdirs();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy-hhmmss");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            cameraIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            imgUri = FileProvider.getUriForFile(MiniBusinessPlanActivity.this,
                    getApplicationContext().getPackageName() + ".provider",
                    new File("/sdcard/jolt/img" + sdf.format(new Date()) + ".jpg"));
        }else {
            imgUri = Uri.fromFile(new File("/sdcard/jolt/img" + sdf.format(new Date()) + ".jpg"));
        }
        //imgUri = Uri.fromFile(new File("/sdcard/sbl/img" + sdf.format(new Date()) + ".jpg"));

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);



        *//*imgUri = Uri.fromFile(new File("/sdcard/sbl/img" + sdf.format(new Date()) + ".jpg"));
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);*//*
    }*/

    private void captureImage() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        (new File(Environment.getExternalStorageDirectory()+"/jolt/")).mkdirs();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy-hhmmss");
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {*/

        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File

        }

        imgUri = FileProvider.getUriForFile(MiniBusinessPlanActivity.this,
                "com.example.android.fileprovider",
                    photoFile);

            /*List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                grantUriPermission(packageName, imgUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }*/
            /*cameraIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cameraIntent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);*/
        /*} else {
            imgUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory()+"/jolt/img" + sdf.format(new Date()) + ".jpg"));
        }*/
        //imgUri = Uri.fromFile(new File("/sdcard/sbl/img" + sdf.format(new Date()) + ".jpg"));

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        imagePath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultcode, Intent data) {
        super.onActivityResult(requestCode, resultcode, data);
        if (requestCode == CAMERA_REQUEST) {
            if (resultcode == RESULT_OK) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;

                //imagePath = imgUri.getPath();

                /*Picasso.with(MainActivity.this)
                        .load(imgUri)
                        .error(R.drawable.vehicledrawable)
                        .into(imageViewVehicleImage);*/
            } else if (resultcode == RESULT_CANCELED) {
                Toast.makeText(MiniBusinessPlanActivity.this,
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(MiniBusinessPlanActivity.this,
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == 5) {
            if (data != null && resultcode == RESULT_OK) {
                Uri selectedImage = data.getData();
                imagePath = getRealPathFromURI(MiniBusinessPlanActivity.this, selectedImage);
                /*Picasso.with(MiniBusinessPlanActivity.this)
                        .load(selectedImage)
                        .error(R.drawable.vehicledrawable)
                        .into(imageViewVehicleImage);*/
            } else {
                Toast.makeText(MiniBusinessPlanActivity.this,
                        "Sorry! Failed to take image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void sendUserRequestData(MultipartEntityBuilder multipartEntityBuilder) {
        final ProgressDialog progressDialog = new ProgressDialog(MiniBusinessPlanActivity.this, R.style.MyTheme);
        progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        MultipartWebServiceCall multipartWebServiceCall = new MultipartWebServiceCall(MiniBusinessPlanActivity.this, new INetworkResponse() {
            @Override
            public void onSuccess(String response) {
                JSONObject jsonObject1 = null;
                try {
                    jsonObject1 = new JSONObject(response);
                    String msg = jsonObject1.getString("message");
                    boolean success = jsonObject1.getBoolean("success");
                    if (success) {
                        Toast.makeText(MiniBusinessPlanActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                        JSONObject jsonObject=jsonObject1.getJSONObject("data");
                        pdfurl=jsonObject.getString("pdfurl");
                        id=jsonObject.getString("id");

                        downloadPdfFile(pdfurl);
                    }else {
                        Toast.makeText(MiniBusinessPlanActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    if (progressDialog.isShowing() || progressDialog != null) {
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(MiniBusinessPlanActivity.this, error, Toast.LENGTH_SHORT).show();
                if (progressDialog.isShowing() || progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        });

        multipartWebServiceCall.execute(multipartEntityBuilder, ServiceUrls.MINI_BUSSINESS_PLAN_URL);
        progressDialog.show();
    }

    private File createFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".pdf",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //imagePath = image.getAbsolutePath();
        return image;
    }

    private void downloadPdfFile(String urlPdf) {
        try {
            fileURI=createFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        AppConstants.uriDoc = FileProvider.getUriForFile(MiniBusinessPlanActivity.this,
                "com.example.android.fileprovider",
                fileURI);

        String extStorageDirectory = Environment.getExternalStorageDirectory()
                .toString();
        File folder = new File(extStorageDirectory, "Jolt");
        if (!folder.exists()) {
            folder.mkdir();
        }
        String fileName = urlPdf.substring(urlPdf.lastIndexOf('/') + 1);
        File file = new File(folder, fileName);
        try {
            if (!file.exists()) {
                file.createNewFile();
                new Downloader(MiniBusinessPlanActivity.this, urlPdf, fileURI,"pdf","Mini Business Plan").execute();
            } else {
                Bundle bundle=new Bundle();
                bundle.putString("url",urlPdf);
                bundle.putString("type","Mini Business Plan");
                Intent intent=new Intent(MiniBusinessPlanActivity.this, MiniPDfViewActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                /*Toast.makeText(MiniBusinessPlanActivity.this, R.string.downloaded, Toast.LENGTH_SHORT).show();
                File file2 = new File(Environment.getExternalStorageDirectory() + "/Jolt/" + fileName);
                PackageManager packageManager = getApplicationContext().getPackageManager();
                Intent testIntent = new Intent(Intent.ACTION_VIEW);
                testIntent.setType("application/pdf");
                List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                Uri uri = Uri.fromFile(file);
                intent.setDataAndType(uri, "application/pdf");
                startActivity(intent);*/
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
