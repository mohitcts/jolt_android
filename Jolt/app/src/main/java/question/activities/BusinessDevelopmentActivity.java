package question.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jolt.R;

public class BusinessDevelopmentActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonBusinessCheckList,buttonMiniBusinessPlan,buttonResources;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_development);

        setupToolbar();
        initialization();
    }


    // setting up toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.business_Development));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }


    private void initialization() {
        buttonBusinessCheckList=(Button)findViewById(R.id.buttonBusinessCheckList);
        buttonMiniBusinessPlan=(Button)findViewById(R.id.buttonMiniBusinessPlan);
        buttonResources=(Button)findViewById(R.id.buttonResources);
        buttonBusinessCheckList.setOnClickListener(this);
        buttonMiniBusinessPlan.setOnClickListener(this);
        buttonResources.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonBusinessCheckList:
                Intent intent = new Intent(this,BusinessCheckList.class);
                startActivity(intent);
                break;
            case R.id.buttonMiniBusinessPlan:
                Intent intent_mini = new Intent(this,MiniBusinessPlanActivity.class);
                startActivity(intent_mini);
                break;
            case R.id.buttonResources:
                startActivity(new Intent(this,ResourcesActivity.class));
                break;
        }
    }
}
