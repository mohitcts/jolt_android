package question.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jolt.R;

public class InstructionActivity extends AppCompatActivity implements View.OnClickListener{
    private ImageView imageViewBack;
    private Button buttonStartNow;
    private String value;
    private TextView textViewContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);
        initializer();
        initListeners();
        getBundleValue();
    }

    private void initializer() {
        buttonStartNow=(Button)findViewById(R.id.buttonStartNow);
        imageViewBack=(ImageView) findViewById(R.id.imageViewBack);
        textViewContent=(TextView) findViewById(R.id.textViewContent);
    }

    private void initListeners() {
        buttonStartNow.setOnClickListener(this);
        imageViewBack.setOnClickListener(this);
    }

    private void getBundleValue() {
        Bundle bundle=new Bundle();
        bundle=getIntent().getExtras();
        value=bundle.getString("value","");
        setValues();
    }

    private void setValues() {
        if(value.equals("0")) {
            textViewContent.setText(R.string.instruction_planning);
        }else if(value.equals("1")){
            textViewContent.setText(R.string.instruction_text);
        }
    }

    @Override
    public void onClick(View v) {
        if(v==imageViewBack){
            onBackPressed();
        }
        if(v==buttonStartNow){
            if(value.equals("0")) {
                startActivity(new Intent(this, PlanningToolsType.class));
            }else if(value.equals("1")){
                DecisiveActivity.page=0;
                startActivity(new Intent(this, DecisiveActivity.class));
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
