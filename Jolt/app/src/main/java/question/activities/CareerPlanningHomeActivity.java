package question.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jolt.R;

public class CareerPlanningHomeActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonCareerPlanner;
    private Button buttonCareerCheckList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_planning_home);

        setupToolbar();
        initialization();
    }


    // setting up toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.career_planning));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }


    private void initialization() {
        buttonCareerPlanner  =(Button)findViewById(R.id.buttonCareerPlanner);
        buttonCareerCheckList=(Button)findViewById(R.id.buttonCareerCheckList);
        buttonCareerPlanner.setOnClickListener(this);
        buttonCareerCheckList.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

      switch (v.getId()){
          case R.id.buttonCareerCheckList:
              Intent intentCheckList= new Intent(this,CareerCheckListActivity.class);
              startActivity(intentCheckList);
              break;
          case R.id.buttonCareerPlanner:
              Intent intent = new Intent(this,CareerPlannerActivity.class);
              startActivity(intent);
              break;
      }
    }
}
