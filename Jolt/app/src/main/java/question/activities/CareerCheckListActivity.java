package question.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jolt.R;

public class CareerCheckListActivity extends AppCompatActivity implements View.OnClickListener {
    private Button buttonNewEmployee,buttonCurrentEmployee,buttonLeader,buttonEmploymentTransition,buttonSetReminder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_check_list);

        setupToolbar();
        initialization();
    }

    private void initialization() {

        buttonCurrentEmployee =(Button)findViewById(R.id.buttonCurrentEmployee);
        buttonSetReminder =(Button)findViewById(R.id.buttonSetReminder);
        buttonNewEmployee =(Button)findViewById(R.id.buttonNewEmployee);
        buttonLeader =(Button)findViewById(R.id.buttonLeader);
        buttonEmploymentTransition =(Button)findViewById(R.id.buttonEmploymentTransition);
        buttonCurrentEmployee.setOnClickListener(this);
        buttonNewEmployee.setOnClickListener(this);
        buttonLeader.setOnClickListener(this);
        buttonEmploymentTransition.setOnClickListener(this);
        buttonSetReminder.setOnClickListener(this);
    }


    // setting up toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.career_checkList));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.buttonEmploymentTransition:
                Intent transitionIntent =new Intent(this,EmploymentTransactionCheckList.class);
                startActivity(transitionIntent);
                break;
            case R.id.buttonNewEmployee:
                Intent intent = new Intent(this,NewEmployeeCheckListActivity.class);
                startActivity(intent);
                break;
            case R.id.buttonLeader:
                Intent intentLeader = new Intent(this,LeaderCheckListActivity.class);
                startActivity(intentLeader);
                break;
            case R.id.buttonCurrentEmployee:
                Intent intent1= new Intent(this,CurrentEmployeeCheckList.class);
                startActivity(intent1);
                break;
            case R.id.buttonSetReminder:
                Intent intentRemindr= new Intent(this,SetReminderActivity.class);
                startActivity(intentRemindr);
                break;
        }
    }
}
