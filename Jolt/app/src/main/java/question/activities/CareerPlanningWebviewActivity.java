package question.activities;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.jolt.R;

public class CareerPlanningWebviewActivity extends AppCompatActivity {
    private WebView webViewCareerPlanning;
    private ProgressDialog progressDialog;
    private String linkURL;
    private String value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_planning_webview);
        getBundleData();
        setupToolbar();
        initializer();

        setWebView();
    }

    private void initializer() {
        webViewCareerPlanning=(WebView)findViewById(R.id.webViewCareerPlanning);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if(value.equals("Career")) {
            mTitle.setText(getResources().getString(R.string.career_advice));
        }else {
            mTitle.setText(getResources().getString(R.string.job_board));
        }
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    private void getBundleData() {
        Bundle bundle = new Bundle();
        bundle = getIntent().getExtras();
        if (bundle != null) {
            linkURL = bundle.getString("linkURL", "");
            value = bundle.getString("value", "");
        }
    }

    private void setWebView() {
        progressDialog = new ProgressDialog(CareerPlanningWebviewActivity.this, R.style.MyTheme);
        progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        webViewCareerPlanning.getSettings().setJavaScriptEnabled(true);
        webViewCareerPlanning.getSettings().setUseWideViewPort(true);
        webViewCareerPlanning.getSettings().setBuiltInZoomControls(true);
        webViewCareerPlanning.loadUrl(linkURL);
        webViewCareerPlanning.setWebViewClient(new AppWebViewClients());
    }

    public class AppWebViewClients extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            progressDialog.dismiss();
        }
    }
}
