package question.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.MainActivity;
import com.jolt.R;

public class GraphActivity2 extends AppCompatActivity {
    private TextView textViewPersonalityStyle,textViewSocial,textViewPersonality,textViewWorkEnvironment,textViewDo,textViewDont;
    private String social,personality,personality_style,work_environment,do_interacting,do_not_interacting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph2);
        setupToolbar();
        initializer();
        getBundleValue();
    }

    private void initializer() {
        textViewPersonalityStyle=(TextView)findViewById(R.id.textViewPersonalityStyle);
        textViewPersonality=(TextView)findViewById(R.id.textViewPersonality);
        textViewSocial=(TextView)findViewById(R.id.textViewSocial);
        textViewWorkEnvironment=(TextView)findViewById(R.id.textViewWorkEnvironment);
        textViewDo=(TextView)findViewById(R.id.textViewDo);
        textViewDont=(TextView)findViewById(R.id.textViewDont);
    }

    private void getBundleValue() {
        try {
            Bundle bundle = new Bundle();
            bundle = getIntent().getExtras();
            if(bundle!=null){
                social=bundle.getString("social");
                personality=bundle.getString("personality");
                personality_style=bundle.getString("personality_style");
                work_environment=bundle.getString("work_environment");
                do_interacting=bundle.getString("do_interacting");
                do_not_interacting=bundle.getString("do_not_interacting");
            }
        } catch (NullPointerException e) {

        }
        setValues();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Attributes");
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up
        return false;
    }

    private void setValues() {
        textViewPersonalityStyle.setText(personality_style);
        textViewPersonality.setText(personality);
        textViewSocial.setText(social);
        textViewWorkEnvironment.setText(work_environment);
        textViewDo.setText(do_interacting);
        textViewDont.setText(do_not_interacting);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radioButtonYes:
                if (checked)
                    //mainActivityCall();
                divert();
                break;
            case R.id.radioButtonNo:
                if (checked)
                    finishAll();
                break;
        }
    }

    public void mainActivityCall() {
        Intent intent = new Intent(GraphActivity2.this, StoreActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void finishAll() {
        Intent intent = new Intent(GraphActivity2.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void divert() {
        Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://35.167.21.236/transcendproserv/product-category/disc-assessment/"));
        startActivity(viewIntent);
    }
}
