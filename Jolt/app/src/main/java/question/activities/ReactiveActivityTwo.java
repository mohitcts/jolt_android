package question.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.jolt.R;

public class ReactiveActivityTwo extends AppCompatActivity {

    private RadioGroup radioQuietGroup;
    private RadioButton radioQuietButton;
    private RadioGroup radioSlowGroup;
    private RadioButton radioSlowButton;
    Button buttonNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reactive_two);

        setUpToolbar();
        initFieldAndListner();
    }


    // initilisation text and button and calling listner
    private void initFieldAndListner(){
        radioQuietGroup = (RadioGroup) findViewById(R.id.radioQuiet);
        radioSlowGroup=(RadioGroup)findViewById(R.id.radioSlow);
        buttonNext = (Button) findViewById(R.id.buttonNext);

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
                int selectedId = radioQuietGroup.getCheckedRadioButtonId();

                int selectedIdSlow= radioSlowGroup.getCheckedRadioButtonId();

                // find the radio button by returned id
                radioQuietButton = (RadioButton) findViewById(selectedId);

                radioSlowButton=(RadioButton)findViewById(selectedIdSlow);

                Log.d(this.getClass().getName(),"RadioValue" + radioQuietButton.getText());
                Log.d(this.getClass().getName(),"RadioValueSlow" + radioSlowButton.getText());


                // Calling next Activity
                startActivity(new Intent(ReactiveActivityTwo.this,GraphActivity.class));
            }
        });
    }


    // setting all the functionality of toolbar
    private void setUpToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.reactiveHome));

        if (toolbar == null) return;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.leftarrow);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }
}
