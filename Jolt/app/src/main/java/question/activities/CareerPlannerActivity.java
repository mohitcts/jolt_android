package question.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import adapters.CareerPlanningAdapter;
import jolt.registration.LoginActivity;
import permissions.MarshmallowPermission;
import permissions.PermissionKeys;
import projections.ItemCareerPlaning;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.DataParser;
import utils.Downloader;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.WebServiceCall;
import utils.WebServiceGet;

public class CareerPlannerActivity extends AppCompatActivity implements View.OnClickListener {
    private Button buttonNext;
    private ListView listViewCareerPlanning;
    private ConnectionDetector connectionDetector;
    private ArrayList<ItemCareerPlaning> careerPlanningArrayList;
    private ArrayList<ItemCareerPlaning> careerPlanning2ArrayList;
    private boolean flag_loading;
    private int selection, count;
    private int mLastFirstVisibleItem;
    private CareerPlanningAdapter careerPlanningAdapter;
    private String landing_page;
    private EditText editTextIdealSelf, editTextIdealAttribute, editTextOtherDevelopmentNeeds, editTextCareerPath, editTextDevelopmentOpportunities, editTextIdealStrength, editTextSpecificCareerGoals10Years, editTextSpecificCareerGoals5Years, editTextSpecificCareerGoals2Years, editTextTraningNeeds;
    private String ideal_self, ideal_attribute, ideal_strengths, training_needs, goals_for_2years, goals_for_5years, goals_for_10years, other_development_needs, career_path, development_opportunities;
    private SharedPreferences sharedpreferences;
    private String user_id, pdfurl, id;
    private MarshmallowPermission marshmallowPermission;
    private File fileURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_plaing);
        marshmallowPermission=new MarshmallowPermission(CareerPlannerActivity.this);
        getUserData();
        setupToolbar();
        initializer();
        initListeners();
        /*careerPlanningArrayList = new ArrayList();
        careerPlanning2ArrayList = new ArrayList<>();
        flag_loading = true;
        selection = 0;
        count = 0;*/
        connectionDetector = new ConnectionDetector(CareerPlannerActivity.this);
        //initializer();


        //getCareerPlanningData(count, selection);
    }

    private void initListeners() {
        buttonNext.setOnClickListener(this);
    }

    private void initializer() {
        editTextIdealSelf = (EditText) findViewById(R.id.editTextIdealSelf);
        editTextIdealAttribute = (EditText) findViewById(R.id.editTextIdealAttribute);
        editTextTraningNeeds = (EditText) findViewById(R.id.editTextTraningNeeds);
        editTextSpecificCareerGoals2Years = (EditText) findViewById(R.id.editTextSpecificCareerGoals2Years);
        editTextSpecificCareerGoals5Years = (EditText) findViewById(R.id.editTextSpecificCareerGoals5Years);
        editTextSpecificCareerGoals10Years = (EditText) findViewById(R.id.editTextSpecificCareerGoals10Years);
        editTextIdealStrength = (EditText) findViewById(R.id.editTextIdealStrength);
        editTextCareerPath = (EditText) findViewById(R.id.editTextCareerPath);
        editTextDevelopmentOpportunities = (EditText) findViewById(R.id.editTextDevelopmentOpportunities);
        editTextOtherDevelopmentNeeds = (EditText) findViewById(R.id.editTextOtherDevelopmentNeeds);
        buttonNext = (Button) findViewById(R.id.buttonNext);

        //  listViewCareerPlanning = (ListView) findViewById(R.id.listViewCareerPlanning);

        /*listViewCareerPlanning.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (view.getId() == listViewCareerPlanning.getId()) {
                    final int currentFirstVisibleItem = listViewCareerPlanning.getFirstVisiblePosition();

                    if (currentFirstVisibleItem > mLastFirstVisibleItem) {
                        //linearLayoutTop.setVisibility(View.GONE);
                        //imageViewAddBottom.setVisibility(View.GONE);
                    } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
                        //linearLayoutTop.setVisibility(View.VISIBLE);
                        //imageViewAddBottom.setVisibility(View.VISIBLE);
                    }
                    mLastFirstVisibleItem = currentFirstVisibleItem;

                }

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getId() == listViewCareerPlanning.getId()) {


                    if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {

                        if (!flag_loading) {
                            Log.e("onScrollccc", "onScrollcc");
                            selection = firstVisibleItem + visibleItemCount - 1;
                            count = count + 1;
                            flag_loading = true;
                            getCareerPlanningData(count, selection);

                        }
                    }

                }
            }

        });*/


    }

    private void getUserData() {
        sharedpreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Keys.ID)) {
            user_id = sharedpreferences.getString(Keys.ID, "");
        }
    }


    // setting up toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.career_planner));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onClick(View v) {
        if (v == buttonNext) {


            ideal_self = editTextIdealSelf.getText().toString();
            ideal_attribute = editTextIdealAttribute.getText().toString();
            training_needs = editTextTraningNeeds.getText().toString();
            goals_for_2years = editTextSpecificCareerGoals2Years.getText().toString();
            goals_for_5years = editTextSpecificCareerGoals5Years.getText().toString();
            goals_for_10years = editTextSpecificCareerGoals10Years.getText().toString();
            ideal_strengths = editTextIdealStrength.getText().toString();
            career_path = editTextCareerPath.getText().toString();
            development_opportunities = editTextDevelopmentOpportunities.getText().toString();
            other_development_needs = editTextOtherDevelopmentNeeds.getText().toString();

            if (ideal_self.length() <= 0) {
                editTextIdealSelf.requestFocus();
                editTextIdealSelf.setHintTextColor(Color.RED);
                Toast.makeText(CareerPlannerActivity.this, "Please Enter Ideal self", Toast.LENGTH_SHORT).show();
            } else if (ideal_attribute.length() <= 0) {
                editTextIdealAttribute.requestFocus();
                editTextIdealAttribute.setHintTextColor(Color.RED);
                Toast.makeText(CareerPlannerActivity.this, "Please Enter Ideal Attribute", Toast.LENGTH_SHORT).show();
            } else if (training_needs.length() <= 0) {
                editTextTraningNeeds.requestFocus();
                editTextTraningNeeds.setHintTextColor(Color.RED);
                Toast.makeText(CareerPlannerActivity.this, "Please Enter Training Needs", Toast.LENGTH_SHORT).show();
            } else if (goals_for_2years.length() <= 0) {
                editTextSpecificCareerGoals2Years.requestFocus();
                editTextSpecificCareerGoals2Years.setHintTextColor(Color.RED);
                Toast.makeText(CareerPlannerActivity.this, "Please Enter Specific Goals for 2 years", Toast.LENGTH_SHORT).show();
            } else if (goals_for_5years.length() <= 0) {
                editTextSpecificCareerGoals5Years.requestFocus();
                editTextSpecificCareerGoals5Years.setHintTextColor(Color.RED);
                Toast.makeText(CareerPlannerActivity.this, "Please Enter Specific Goals for 5 years", Toast.LENGTH_SHORT).show();
            } else if (goals_for_10years.length() <= 0) {
                editTextSpecificCareerGoals10Years.requestFocus();
                editTextSpecificCareerGoals10Years.setHintTextColor(Color.RED);
                Toast.makeText(CareerPlannerActivity.this, "Please Enter Specific Goals for 10 years", Toast.LENGTH_SHORT).show();
            } else if (ideal_strengths.length() <= 0) {
                editTextIdealStrength.requestFocus();
                editTextIdealStrength.setHintTextColor(Color.RED);
                Toast.makeText(CareerPlannerActivity.this, "Please Enter Ideal strengths", Toast.LENGTH_SHORT).show();
            } else if (career_path.length() <= 0) {
                editTextCareerPath.requestFocus();
                editTextCareerPath.setHintTextColor(Color.RED);
                Toast.makeText(CareerPlannerActivity.this, "Please Enter Career Path", Toast.LENGTH_SHORT).show();
            } else if (development_opportunities.length() <= 0) {
                editTextDevelopmentOpportunities.requestFocus();
                editTextDevelopmentOpportunities.setHintTextColor(Color.RED);
                Toast.makeText(CareerPlannerActivity.this, "Please Enter Development oppurtunities", Toast.LENGTH_SHORT).show();
            } else if (other_development_needs.length() <= 0) {
                editTextOtherDevelopmentNeeds.requestFocus();
                editTextOtherDevelopmentNeeds.setHintTextColor(Color.RED);
                Toast.makeText(CareerPlannerActivity.this, "Please Enter Other Development Needs", Toast.LENGTH_SHORT).show();
            } else {
                String[] permissions = {PermissionKeys.PERMISSION_WRITE_EXTERNAL_STORAGE, PermissionKeys.PERMISSION_READ_EXTERNAL_STORAGE};
                if (marshmallowPermission.isPermissionGrantedAll(PermissionKeys.REQUEST_CODE_PERMISSION_ALL, permissions)) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("user_id", user_id);
                        jsonObject.put("ideal_self", ideal_self);
                        jsonObject.put("ideal_attribute", ideal_attribute);
                        jsonObject.put("training_needs", training_needs);
                        jsonObject.put("goals_for_2years", goals_for_2years);
                        jsonObject.put("goals_for_5years", goals_for_5years);
                        jsonObject.put("goals_for_10years", goals_for_10years);
                        jsonObject.put("ideal_strengths", ideal_strengths);
                        jsonObject.put("career_path", career_path);
                        jsonObject.put("development_opportunities", development_opportunities);
                        jsonObject.put("other_development_needs", other_development_needs);
                    } catch (JSONException e) {

                    }

                    submitData(jsonObject);
                }
            }
            }
        }

    private void submitData(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(CareerPlannerActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(CareerPlannerActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    JSONObject jsonObject1 = null;
                    try {
                        jsonObject1 = new JSONObject(response);
                        String msg = jsonObject1.getString("message");
                        boolean success = jsonObject1.getBoolean("success");
                        if (success) {
                            Toast.makeText(CareerPlannerActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                            JSONObject jsonObject = jsonObject1.getJSONObject("data");
                            pdfurl = jsonObject.getString("pdfurl");
                            id = jsonObject.getString("id");

                            downloadPdfFile(pdfurl);
                        } else {
                            Toast.makeText(CareerPlannerActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        if (progressDialog.isShowing() || progressDialog != null) {
                            progressDialog.dismiss();
                        }
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.CAREER_PLANNER_URL);
            progressDialog.show();
        } else {
            Toast.makeText(CareerPlannerActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    private File createFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".pdf",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //imagePath = image.getAbsolutePath();
        return image;
    }

    private void downloadPdfFile(String urlPdf) {
        try {
            fileURI=createFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        AppConstants.uriDoc = FileProvider.getUriForFile(CareerPlannerActivity.this,
                "com.example.android.fileprovider",
                fileURI);

        String extStorageDirectory = Environment.getExternalStorageDirectory()
                .toString();
        File folder = new File(extStorageDirectory, "Jolt");
        if (!folder.exists()) {
            folder.mkdir();
        }
        String fileName = urlPdf.substring(urlPdf.lastIndexOf('/') + 1);
        File file = new File(folder, fileName);
        try {
            if (!file.exists()) {
                file.createNewFile();
                new Downloader(CareerPlannerActivity.this, urlPdf, fileURI, "pdf", "Career Planner").execute();
            } else {
                Bundle bundle = new Bundle();
                bundle.putString("url", urlPdf);
                bundle.putString("type", "Career Planner");
                Intent intent = new Intent(CareerPlannerActivity.this, MiniPDfViewActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                /*Toast.makeText(MiniBusinessPlanActivity.this, R.string.downloaded, Toast.LENGTH_SHORT).show();
                File file2 = new File(Environment.getExternalStorageDirectory() + "/Jolt/" + fileName);
                PackageManager packageManager = getApplicationContext().getPackageManager();
                Intent testIntent = new Intent(Intent.ACTION_VIEW);
                testIntent.setType("application/pdf");
                List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                Uri uri = Uri.fromFile(file);
                intent.setDataAndType(uri, "application/pdf");
                startActivity(intent);*/
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private void getCareerPlanningData(int pageCount, final int selection) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(CareerPlannerActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            WebServiceGet webServiceGet = new WebServiceGet(CareerPlannerActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        if (!response.equals("")) {
                            JSONObject jsonObject = new JSONObject(response);
                            int beforeSize = careerPlanningArrayList.size();
                            JSONArray jsonArrayCategories = jsonObject.getJSONArray("results");
                            if (jsonArrayCategories.length() > 0) {
                                DataParser dataParser = new DataParser();
                                careerPlanning2ArrayList = dataParser.parseCareerPlanningData(jsonArrayCategories);
                                careerPlanningArrayList.addAll(careerPlanning2ArrayList);
                                int afterSize = careerPlanningArrayList.size();
                                if (beforeSize != afterSize) {
                                    careerPlanningAdapter = new CareerPlanningAdapter(CareerPlannerActivity.this, careerPlanningArrayList);
                                    listViewCareerPlanning.setAdapter(careerPlanningAdapter);
                                    flag_loading = false;
                                    listViewCareerPlanning.setSelection(selection);
                                } else {
                                    flag_loading = true;
                                    listViewCareerPlanning.setSelection(selection);
                                }

                                listViewCareerPlanning.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        JSONObject jsonObjectRefs = careerPlanningArrayList.get(position).getRefs();
                                        try {
                                            landing_page = jsonObjectRefs.getString("landing_page");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Bundle bundle = new Bundle();
                                        bundle.putString("linkURL", landing_page);
                                        Intent intent = new Intent(CareerPlannerActivity.this, CareerPlanningWebviewActivity.class);
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }
                                });
                            }
                        } else {
                            Toast.makeText(CareerPlannerActivity.this, "Something went wrong.Try Again.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onError(String error) {
                    flag_loading = false;
                    count = count - 1;
                    progressDialog.dismiss();
                    Toast.makeText(CareerPlannerActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceGet.execute(ServiceUrls.CAREER_PLANNING_URL + count);
        } else {
            Toast.makeText(CareerPlannerActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }
}
