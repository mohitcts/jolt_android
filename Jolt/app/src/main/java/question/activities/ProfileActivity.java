package question.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.MainActivity;
import com.jolt.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import fcm.DeleteTokenService;
import jolt.registration.LoginActivity;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.WebServiceCall;


public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textViewEditProfile, UserNameText, UserEmailText, UserMobileText, logOutTextView;
    private SharedPreferences sharedPreferences;
    private String userName, userEmail, userMobile;
    private TextView UserCityText, UserDegreeText, UserAgeText;
    private ConnectionDetector connectionDetector;
    private ImageView imageViewUser;
    private String userId;
    private String json_userName;
    private String image_url;
    private String json_email;
    private String json_mobile;
    private String json_city;
    private String json_graduation;
    private String json_age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        connectionDetector = new ConnectionDetector(this);

        setupToolbar();
        initialization();
        getSharedPreferencesData();


    }

    private void getSharedPreferencesData() {

        sharedPreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, MODE_PRIVATE);
        if (sharedPreferences.contains(Keys.NAME)) {
            userId = sharedPreferences.getString(Keys.ID, null);
            userName = sharedPreferences.getString(Keys.NAME, null);
            userEmail = sharedPreferences.getString(Keys.EMAIL, null);
            userMobile = sharedPreferences.getString(Keys.PHONE, null);
            UserNameText.setText(userName);
            Log.d(this.getClass().getName(), "USER_ID" + userId);

            if (!userId.equals("") && !userId.equals(null)) {
                // Getting user Information call getUserProfile WebService API
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Keys.USERID, userId);
                    // jsonObject.put(Keys.USERID,"8");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getUserProfile(jsonObject);
            }
        }
    }

    private void initialization() {
        textViewEditProfile = (TextView) findViewById(R.id.textViewEditProfile);
        textViewEditProfile.setOnClickListener(this);
        UserNameText = (TextView) findViewById(R.id.UserNameText);
        UserEmailText = (TextView) findViewById(R.id.UserEmailText);
        UserMobileText = (TextView) findViewById(R.id.UserMobileText);
        logOutTextView = (TextView) findViewById(R.id.logOutTextView);
        UserCityText = (TextView) findViewById(R.id.UserCityText);
        UserDegreeText = (TextView) findViewById(R.id.UserDegreeText);
        UserAgeText = (TextView) findViewById(R.id.UserAgeText);
        imageViewUser = (ImageView) findViewById(R.id.imageViewUser);
        logOutTextView.setOnClickListener(this);

    }


    // setting all the functionality of toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.profileHome));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textViewEditProfile:
                if (!json_userName.equals("") && !json_userName.equals("")) {

                    Bundle b = new Bundle();
                    b.putString(Keys.NAME, json_userName);
                    b.putString(Keys.MOBILE, json_mobile);
                    b.putString(Keys.EMAIL, json_email);
                    b.putString(Keys.CITY, json_city);
                    b.putString(Keys.AGE, json_age);
                    b.putString(Keys.GRADUATION, json_graduation);
                    if (!image_url.equals("") && !image_url.equals(null)) {
                        b.putString(Keys.PHOTO, image_url);
                    }
                    Intent intent = new Intent(this, EditProfileActivity.class);
                    intent.putExtras(b);
                    startActivity(intent);
                }
                break;
            case R.id.logOutTextView:
                logoutPopup();
                break;
        }
    }

    private void logoutPopup() {
        new AlertDialog.Builder(this)
                .setTitle("Logout")
                .setMessage("Would you like to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // logout method call
                        logout();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void logout() {
        Intent intents=new Intent(ProfileActivity.this, DeleteTokenService.class);
        startService(intents);

        getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, MODE_PRIVATE).edit().clear().commit();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        // startActivity(new Intent(this, LoginActivity.class));
    }


    private void getUserProfile(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(ProfileActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(ProfileActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        Log.d(this.getClass().getName(), "GET_PROFILE_RESPONSE" + response);
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        String message = jsonObjectResponse.getString("message");
                        if (success) {
                            // Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            JSONObject jsonObject = jsonObjectResponse.getJSONObject(Keys.DATA);
                            json_userName = jsonObject.getString(Keys.NAME);
                            json_email = jsonObject.getString(Keys.EMAIL);
                            json_mobile = jsonObject.getString(Keys.MOBILE);
                            json_city = jsonObject.getString(Keys.CITY);
                            json_graduation = jsonObject.getString(Keys.GRADUATION);
                            json_age = jsonObject.getString(Keys.AGE);
                            image_url = jsonObject.getString(Keys.PHOTO);

                            UserNameText.setText(json_userName);
                            UserEmailText.setText(json_email);
                            UserMobileText.setText(json_mobile);
                            UserAgeText.setText(json_age);
                            UserDegreeText.setText(json_graduation);
                            UserCityText.setText(json_city);

                            if (!image_url.equals("") && !image_url.equals(null)) {
                                imageViewUser.setVisibility(View.VISIBLE);
                                Picasso.with(getApplicationContext()).load(image_url).placeholder(R.drawable.thumbnail).into(imageViewUser);
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.GET_USER_PROFILE);
            progressDialog.show();
        } else {
            Toast.makeText(ProfileActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }


}
