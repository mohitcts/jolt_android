package question.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fragments.DecisiveQuestionFragment;
import fragments.ReactiveQuestionFragment;
import jolt.registration.LoginActivity;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.WebServiceCall;

public class DecisiveActivity extends AppCompatActivity {

    private RadioGroup radioQuietGroup;
    private RadioButton radioQuietButton;
    private RadioGroup radioSlowGroup;
    private RadioButton radioSlowButton;
    private ConnectionDetector connectionDetector;
    Button buttonNext;
    public static int page=0;
    public ArrayList<String> questionIdArrayList;
    public ArrayList<String> rateArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decisive);
        connectionDetector = new ConnectionDetector(this);
        questionIdArrayList=new ArrayList<>();
        rateArrayList=new ArrayList<>();

        setupToolbar();

        Bundle bundle=new Bundle();
        bundle.putInt("page",page);

        Fragment fragment = new DecisiveQuestionFragment();
        addFragment(fragment, bundle);
    }


    /**
     * This method is used to add fragment more fragments to the activity when there is a need.
     */
    public void addFragment(Fragment fragment, Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        fragment.setArguments(bundle);
        fm.beginTransaction()
                .add(R.id.frame_container, fragment).addToBackStack(null).commit();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.decisiveHome));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        FragmentManager fm1 = getSupportFragmentManager();
        if (fm1.getBackStackEntryCount() > 1) {
            fm1.popBackStack();
            page--;
        } else {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm1 = getSupportFragmentManager();
        if (fm1.getBackStackEntryCount() > 1) {
            fm1.popBackStack();
            page--;
        } else {
            finish();
        }
    }

}
