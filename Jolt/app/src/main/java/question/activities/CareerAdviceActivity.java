package question.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.CareerPlanningAdapter;
import adapters.JobBoardAdapter;
import projections.ItemCareerAdvice;
import projections.ItemCareerPlaning;
import utils.ConnectionDetector;
import utils.DataParser;
import utils.INetworkResponse;
import utils.ServiceUrls;
import utils.WebServiceGet;

public class CareerAdviceActivity extends AppCompatActivity {

    private ListView listViewCareerAdvice;
    private ConnectionDetector connectionDetector;
    private ArrayList<ItemCareerPlaning> careerPlanningArrayList;
    private ArrayList<ItemCareerPlaning> careerPlanning2ArrayList;
    private boolean flag_loading;
    private int selection, count;
    private int mLastFirstVisibleItem;
    private CareerPlanningAdapter careerPlanningAdapter;
    private String landing_page;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_advice);
        connectionDetector=new ConnectionDetector(CareerAdviceActivity.this);
        careerPlanningArrayList= new ArrayList<>();
        careerPlanning2ArrayList= new ArrayList<>();

        flag_loading=true;
        selection=0;
        count=0;
        setupToolbar();
        initialization();
        getCareerAdviceData(count,selection);
    }


    // setting up toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.career_advice));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }


    private void initialization() {
        listViewCareerAdvice=(ListView)findViewById(R.id.listViewCareerAdvice);
        listViewCareerAdvice.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (view.getId()== listViewCareerAdvice.getId()){
                    final  int currentFirstVisibleItem=listViewCareerAdvice.getFirstVisiblePosition();
                    mLastFirstVisibleItem= currentFirstVisibleItem;
                    //if ()
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (view.getId()==listViewCareerAdvice.getId()){

                    if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount !=0){

                        if (!flag_loading){
                            Log.e("OnScrollList","Scrolling..");
                            selection = firstVisibleItem + visibleItemCount-1;
                            count=count+1;
                            flag_loading=true;
                            getCareerAdviceData(count,selection);
                        }
                    }
                }
            }
        });




    }

    private void getCareerAdviceData(int pageCount, final int selection) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(CareerAdviceActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            WebServiceGet webServiceGet = new WebServiceGet(CareerAdviceActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        if (!response.equals("")) {
                            JSONObject jsonObject = new JSONObject(response);
                            int beforeSize = careerPlanningArrayList.size();
                            JSONArray jsonArrayCategories = jsonObject.getJSONArray("results");
                            if (jsonArrayCategories.length() > 0) {
                                DataParser dataParser = new DataParser();
                                careerPlanning2ArrayList = dataParser.parseCareerPlanningData(jsonArrayCategories);
                                careerPlanningArrayList.addAll(careerPlanning2ArrayList);
                                int afterSize = careerPlanningArrayList.size();
                                if (beforeSize != afterSize) {
                                    careerPlanningAdapter = new CareerPlanningAdapter(CareerAdviceActivity.this, careerPlanningArrayList);
                                    listViewCareerAdvice.setAdapter(careerPlanningAdapter);
                                    flag_loading = false;
                                    listViewCareerAdvice.setSelection(selection);
                                } else {
                                    flag_loading = true;
                                    listViewCareerAdvice.setSelection(selection);
                                }

                                listViewCareerAdvice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        JSONObject jsonObjectRefs=careerPlanningArrayList.get(position).getRefs();
                                        try {
                                            landing_page=jsonObjectRefs.getString("landing_page");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Bundle bundle=new Bundle();
                                        bundle.putString("linkURL",landing_page);
                                        bundle.putString("value","Career");
                                        Intent intent=new Intent(CareerAdviceActivity.this,CareerPlanningWebviewActivity.class);
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }
                                });
                            }
                        } else {
                            Toast.makeText(CareerAdviceActivity.this, "Something went wrong.Try Again.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onError(String error) {
                    flag_loading = false;
                    count = count - 1;
                    progressDialog.dismiss();
                    Toast.makeText(CareerAdviceActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceGet.execute(ServiceUrls.CAREER_PLANNING_URL + count);
        } else {
            Toast.makeText(CareerAdviceActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }
}
