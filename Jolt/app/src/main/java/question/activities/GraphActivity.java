package question.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.jolt.MainActivity;
import com.jolt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jolt.registration.LoginActivity;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.WebServiceCall;

public class GraphActivity extends AppCompatActivity implements View.OnClickListener{
    RadarChart chart;
    RadioButton radioButtonYes;
    private SharedPreferences sharedpreferences;
    private String user_name,user_id="";
    private ConnectionDetector connectionDetector;
    private String personality;
    private String personality_style,social,do_interacting,do_not_interacting,work_environment;
    private TextView textViewPersonalityStyle;
    private Button buttonNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        connectionDetector=new ConnectionDetector(this);

        setupToolbar();
        initFieldsAndListner();
        getUserData();


    }

    private void getUserData() {
        sharedpreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Keys.NAME)) {
            user_name = sharedpreferences.getString(Keys.NAME, "");
            user_id = sharedpreferences.getString(Keys.ID, "");
        }
        JSONObject jsonObject1=new JSONObject();
        try {
            jsonObject1.put("user_id",user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        getAssesmentValue(jsonObject1);
    }


    private void getAssesmentValue(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(GraphActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(GraphActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        if (success) {
                            //JSONObject jsonObject = jsonObjectResponse.getJSONObject(Keys.DATA);
                            JSONArray jsonArray=jsonObjectResponse.getJSONArray(Keys.DATA);
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                String maxreactive=jsonObject1.getString("maxreactive");
                                String maxdecisive=jsonObject1.getString("maxdecisive");
                                String reactive_ratings=jsonObject1.getString("reactive_ratings");
                                String decisive_ratings=jsonObject1.getString("decisive_ratings");
                                personality_style=jsonObject1.getString("personality_style");
                                personality=jsonObject1.getString("personality");
                                social=jsonObject1.getString("social");
                                work_environment=jsonObject1.getString("work_environment");
                                do_interacting=jsonObject1.getString("do_interacting");
                                do_not_interacting=jsonObject1.getString("do_not_interacting");

                                textViewPersonalityStyle.setText("Your personality style is "+personality_style);
                            }
                            charPlotted();

                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.ASSESMENT_PERSONALITY_URL);
            progressDialog.show();
        } else {
            Toast.makeText(GraphActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }


    // initilisation filed here
    private void initFieldsAndListner() {
        radioButtonYes = (RadioButton) findViewById(R.id.radioButtonYes);
        buttonNext = (Button) findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(this);
        textViewPersonalityStyle = (TextView) findViewById(R.id.textViewPersonalityStyle);
    }


    public void mainActivityCall() {
        Intent intent = new Intent(GraphActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void charPlotted() {
        chart = (RadarChart) findViewById(R.id.chart);

        ArrayList<Entry> entries = new ArrayList<>();

        entries.add(new Entry(5f, 0));

        if(personality_style.equals("Leader")) {
            entries.add(new Entry(7f, 1));
        }else {
            entries.add(new Entry(5f, 1));
        }

        entries.add(new Entry(5f, 2));

        if(personality_style.equals("Visionary")) {
            entries.add(new Entry(7f, 1));
        }else {
            entries.add(new Entry(5f, 1));
        }

        entries.add(new Entry(5f, 3));

        if(personality_style.equals("Stable")) {
            entries.add(new Entry(7f, 1));
        }else {
            entries.add(new Entry(5f, 1));
        }

        entries.add(new Entry(5f, 4));

        if(personality_style.equals("Diligent")) {
            entries.add(new Entry(7f, 1));
        }else {
            entries.add(new Entry(5f, 1));
        }
        /*entries.add(new Entry(5f, 5));
        entries.add(new Entry(1f, 4));
        entries.add(new Entry(8f, 4));*/

       /* ArrayList<Entry> entries2 = new ArrayList<>();
        entries2.add(new Entry(1f, 0));
        entries2.add(new Entry(5f, 1));
        entries2.add(new Entry(6f, 2));
        entries2.add(new Entry(3f, 3));
        entries2.add(new Entry(4f, 4));
        entries2.add(new Entry(8f, 5));*/

        RadarDataSet dataset_comp1 = new RadarDataSet(entries, "");

        // RadarDataSet dataset_comp2 = new RadarDataSet(entries2, "");

        dataset_comp1.setColor(Color.CYAN);
        dataset_comp1.setDrawFilled(true);


        //  dataset_comp2.setColor(Color.RED);
        //  dataset_comp2.setDrawFilled(true);


        ArrayList<RadarDataSet> dataSets = new ArrayList<RadarDataSet>();
        dataSets.add(dataset_comp1);
        //  dataSets.add(dataset_comp2);

        ArrayList<String> labels = new ArrayList<String>();
        labels.add("Low Reactive");
        labels.add("Leader");
        labels.add("High Decisive");
        labels.add("Visionary");
        labels.add("High Reactive");
        labels.add("Stable");
        labels.add("Low Decisive");
        labels.add("Diligent");


        RadarData data = new RadarData(labels, dataSets);
        chart.setData(data);
        chart.setDescriptionColor(ContextCompat.getColor(this, R.color.white));
        //String description = "Employee-Skill Analysis (scale of 1-10), 10 being the highest";
        // chart.setDescription(description);
        // chart.setWebLineWidthInner(0.5f);
        //chart.setDescriptionColor(Color.RED);

        //chart.setSkipWebLineCount(10);

        chart.invalidate();
        chart.animate();
    }


    // setting all the functionality of toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.info_Graph));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up
        return false;
    }

    @Override
    public void onClick(View v) {
        if(v==buttonNext){
            Bundle bundle=new Bundle();
            bundle.putString("social",social);
            bundle.putString("personality",personality);
            bundle.putString("personality_style",personality_style);
            bundle.putString("work_environment",work_environment);
            bundle.putString("do_interacting",do_interacting);
            bundle.putString("do_not_interacting",do_not_interacting);
            Intent intent=new Intent(GraphActivity.this,GraphActivity2.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }
}
