package question.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import adapters.JobBoardAdapter;
import adapters.CareerPlanningAdapter;
import fragments.DecisiveQuestionFragment;
import fragments.JobBoardFragment;
import projections.ItemCareerAdvice;
import utils.ConnectionDetector;
import utils.DataParser;
import utils.INetworkResponse;
import utils.ServiceUrls;
import utils.WebServiceGet;

public class JobBoardActivity extends AppCompatActivity implements View.OnClickListener {

    private ConnectionDetector connectionDetector;
    private ListView listViewJobBoard;
    private boolean flag_loading;
    private int selection, count;
    private int mLastFirstVisibleItem;
    private CareerPlanningAdapter careerPlanningAdapter;
    private String landing_page;
    private ArrayList<ItemCareerAdvice> careerAdvice2ArrayList;
    private ArrayList<ItemCareerAdvice> careerAdviceArrayList;
    private JobBoardAdapter jobBoardAdapter;
    private TextView textViewSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_board);
        connectionDetector = new ConnectionDetector(JobBoardActivity.this);

        setupToolbar();
        Bundle bundle=new Bundle();
        bundle.putString("url","");
        Fragment fragment = new JobBoardFragment();
        addFragment(fragment, bundle);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        if (toolbar != null) {
            TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
            mTitle.setText(getResources().getString(R.string.job_board));
            setSupportActionBar(toolbar);
            final ActionBar ab = getSupportActionBar();
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    public void addFragment(Fragment fragment,Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        fragment.setArguments(bundle);
        fm.beginTransaction()
                .add(R.id.frame_container, fragment).addToBackStack(null).commit();
    }


    @Override
    public void onClick(View v) {

    }
}
