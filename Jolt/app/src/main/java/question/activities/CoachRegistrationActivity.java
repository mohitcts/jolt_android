package question.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import projections.ItemCoachcategories;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.DataParser;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.WebServiceCall;
import utils.WebServiceGet;

public class CoachRegistrationActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener{
    private EditText editTextFirstName,editTextExperience,edittextLastName,editTextEmailAddress,editTextMobile,editTextAboutYourself;
    private Button buttonSubmit;
    private Spinner spinnerCategory;
    private ConnectionDetector connectionDetector;
    private ArrayList<ItemCoachcategories> coachCategoryArrayList;
    private ArrayList<String> categoryNameArrayList;
    private String cat_id;
    private String firstName,lastName,email,mobile,experience,user_id="",aboutYourself;
    private SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_registration);
        connectionDetector=new ConnectionDetector(CoachRegistrationActivity.this);

        getUserData();
        setupToolbar();
        initializer();
        initListeners();
        getCoachCategories();
    }

    private void initializer() {
        editTextFirstName=(EditText)findViewById(R.id.editTextFirstName);
        edittextLastName=(EditText)findViewById(R.id.edittextLastName);
        editTextEmailAddress=(EditText)findViewById(R.id.editTextEmailAddress);
        editTextMobile=(EditText)findViewById(R.id.editTextMobile);
        editTextAboutYourself=(EditText)findViewById(R.id.editTextAboutYourself);
        editTextExperience=(EditText)findViewById(R.id.editTextExperience);
        buttonSubmit=(Button)findViewById(R.id.buttonSubmit);
        spinnerCategory=(Spinner) findViewById(R.id.spinnerCategory);
    }

    private void getUserData() {
        sharedpreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Keys.NAME)) {
            user_id = sharedpreferences.getString(Keys.ID, "");
        }
    }


    private void initListeners() {
        buttonSubmit.setOnClickListener(this);
    }

    // setting all the functionality of toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.coach_registration));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up
        return false;
    }

    @Override
    public void onClick(View v) {
        if(v==buttonSubmit){
            firstName=editTextFirstName.getText().toString();
            lastName=edittextLastName.getText().toString();
            email=editTextEmailAddress.getText().toString();
            mobile=editTextMobile.getText().toString();
            experience=editTextExperience.getText().toString();
            aboutYourself=editTextAboutYourself.getText().toString();

            if(firstName.length()==0){
                editTextFirstName.setError("Please Enter First Name");
                editTextFirstName.requestFocus();
                Toast.makeText(CoachRegistrationActivity.this, "Please Enter First Name", Toast.LENGTH_SHORT).show();
            }else if(lastName.length()==0){
                edittextLastName.setError("Please Enter Last Name");
                edittextLastName.requestFocus();
                Toast.makeText(CoachRegistrationActivity.this, "Please Enter Last Name", Toast.LENGTH_SHORT).show();
            }else if(email.length()==0){
                editTextEmailAddress.setError("Please Enter Email Address");
                editTextEmailAddress.requestFocus();
                Toast.makeText(CoachRegistrationActivity.this, "Please Enter Email Address", Toast.LENGTH_SHORT).show();
            }else if(mobile.length()==0){
                editTextMobile.setError("Please Enter Mobile");
                editTextMobile.requestFocus();
                Toast.makeText(CoachRegistrationActivity.this, "Please Enter Mobile", Toast.LENGTH_SHORT).show();
            }else if(experience.length()==0){
                editTextExperience.setError("Please Enter Experience");
                editTextExperience.requestFocus();
                Toast.makeText(CoachRegistrationActivity.this, "Please Enter Experience", Toast.LENGTH_SHORT).show();
            }else if(aboutYourself.length()==0){
                editTextAboutYourself.setError("Please Enter About Yourself");
                editTextAboutYourself.requestFocus();
                Toast.makeText(CoachRegistrationActivity.this, "Please Enter About Yourself", Toast.LENGTH_SHORT).show();
            } else {
                JSONObject jsonObject=new JSONObject();
                try {
                    jsonObject.put("first_name",firstName);
                    jsonObject.put("last_name",lastName);
                    jsonObject.put("mobile",mobile);
                    jsonObject.put("experience",experience);
                    jsonObject.put("email",email);
                    jsonObject.put("user_id",user_id);
                    jsonObject.put("category",cat_id);
                    jsonObject.put("about_your_self",aboutYourself);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                submitCoachData(jsonObject);
            }
        }

    }

    private void getCoachCategories() {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(CoachRegistrationActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            WebServiceGet webServiceGet = new WebServiceGet(CoachRegistrationActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        if (!response.equals("")) {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean success = jsonObject.getBoolean("success");
                            //String message = jsonObject.getString("msg");
                            if (success) {
                                JSONArray jsonArrayCategories = jsonObject.getJSONArray("data");
                                if (jsonArrayCategories.length() > 0) {
                                    DataParser dataParser = new DataParser();
                                    coachCategoryArrayList = new ArrayList<ItemCoachcategories>();
                                    coachCategoryArrayList = dataParser.parseCoachCategoryData(jsonArrayCategories);
                                    categoryNameArrayList = new ArrayList<>();
                                    for (int i = 0; i < coachCategoryArrayList.size(); i++) {
                                        categoryNameArrayList.add(coachCategoryArrayList.get(i).getCategory_name());
                                    }
                                    initSpinnerCategories();
                                    /*homeCategoriesAdapter = new HomeCategoryAdapter(getActivity(), homeCategoriesArrayList);
                                    listViewHomeCategories.setAdapter(homeCategoriesAdapter);*/
                                }
                            }
                        } else {
                            Toast.makeText(CoachRegistrationActivity.this, "Something went wrong.Try Again.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(CoachRegistrationActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceGet.execute(ServiceUrls.GET_COACH_CATEGORY_URL);
        } else {
            Toast.makeText(CoachRegistrationActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void initSpinnerCategories() {
        spinnerCategory = (Spinner) findViewById(R.id.spinnerCategory);
        spinnerCategory.setOnItemSelectedListener(this);
        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categoryNameArrayList);
        // Drop down layout style - list view with radio button
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinnerCategory.setAdapter(categoryAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.spinnerCategory:
                cat_id = coachCategoryArrayList.get(position).getCatid();
                break;

        }
    }

    public void onNothingSelected(AdapterView arg0) {
        // TODO Auto-generated method stub
    }


    private void submitCoachData(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(CoachRegistrationActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(CoachRegistrationActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        Log.d(this.getClass().getName(),"LOGIN_RESPONSE" + response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        String message = jsonObjectResponse.getString("message");
                        if (success) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();


                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.REGISTER_COACH_URL);
            progressDialog.show();
        } else {
            Toast.makeText(CoachRegistrationActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }
}
