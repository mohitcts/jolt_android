package question.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jolt.R;

public class CoachTypeActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonCareerAdvice;
    private Button buttonFindARecruiter;
    private Button buttonBlog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_type);

        // calling toolbar
        setupToolbar();
        initListner();
    }


    private void initListner() {
        buttonCareerAdvice = (Button) findViewById(R.id.buttonCareerAdvice);
        buttonFindARecruiter = (Button) findViewById(R.id.buttonFindARecruiter);
        buttonBlog = (Button) findViewById(R.id.buttonBlog);
        buttonBlog.setOnClickListener(this);
        buttonCareerAdvice.setOnClickListener(this);
        buttonFindARecruiter.setOnClickListener(this);
    }

    // setting all the functionality of toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.coach_type));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonCareerAdvice:
                Intent intent = new Intent(this, CareerAdviceActivity.class);
                startActivity(intent);
                break;
            case R.id.buttonFindARecruiter:
                startActivity(new Intent(this, FindARecruiterActivity.class));
                break;
            case R.id.buttonBlog:
                //openBlogWebView();
                startActivity(new Intent(this, BlogWebViewactivity.class));
                break;
        }
    }

    private void openBlogWebView() {
        Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://notmymothertongue.com/"));
        startActivity(viewIntent);
    }

}
