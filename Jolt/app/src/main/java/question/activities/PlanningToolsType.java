package question.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jolt.R;

public class PlanningToolsType extends AppCompatActivity implements View.OnClickListener {

    private Button buttonCarrerPlanning,buttonBusinessDevelopment;
    private Button backButton;
    private Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planning_tools_type);

        // calling toolbar
        setupToolbar();
        initListner();
    }


    private void initListner(){
        buttonCarrerPlanning=(Button)findViewById(R.id.buttonCarrerPlanning);
        buttonBusinessDevelopment=(Button)findViewById(R.id.buttonBusinessDevelopment);
     //   buttonResources=(Button)findViewById(R.id.buttonResources);
      //  buttonResources.setOnClickListener(this);
        buttonBusinessDevelopment.setOnClickListener(this);
        buttonCarrerPlanning.setOnClickListener(this);
       // backButton=(Button)findViewById(R.id.backButton);
       // nextButton=(Button)findViewById(R.id.nextButton);
       // backButton.setOnClickListener(this);
      //  nextButton.setOnClickListener(this);
    }

    // setting all the functionality of toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("PLANNING TOOLS");
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonCarrerPlanning:

                startActivity(new Intent(this, CareerPlanningHomeActivity.class));
                break;

            case R.id.buttonBusinessDevelopment:

                startActivity(new Intent(this, BusinessDevelopmentActivity.class));
                break;

            case R.id.buttonResources:
              //  startActivity(new Intent(this,ResourcesActivity.class));
                break;
           /* case R.id.nextButton:

                break;*/
        }
    }
}
