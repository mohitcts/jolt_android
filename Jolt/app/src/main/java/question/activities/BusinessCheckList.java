package question.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.BusinessCheckListAdapter;
import adapters.CareerPlanningAdapter;
import jolt.registration.LoginActivity;
import projections.ItemBussinessChecklist;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.DataParser;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.WebServiceCall;
import utils.WebServiceGet;

public class BusinessCheckList extends AppCompatActivity implements View.OnClickListener {
    private ListView listViewBussinessCheckList;

    private Button buttonNext;
    private ConnectionDetector connectionDetector;
    private ArrayList<ItemBussinessChecklist> bussinessCheckListArrayList;
    private BusinessCheckListAdapter businessCheckListAdapter;
    private ArrayList<String> checkedArraylist;
    private SharedPreferences sharedpreferences;
    private String user_id;
    private ArrayList<Integer> checkboxListStatus;
    private ArrayList<String> checkedArrayListInitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_check_list);
        connectionDetector = new ConnectionDetector(BusinessCheckList.this);
        checkboxListStatus=new ArrayList<>();
        checkedArrayListInitial=new ArrayList<>();
        // calling toolbar showing headings
        setupToolbar();
        initFieldListner();
        getUserData();
        getAllList();
    }

    private void getAllList() {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("user_id",user_id);
        } catch (JSONException e) {

        }
        getBusinessCheckListData(jsonObject);
    }


    private void getUserData() {
        sharedpreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Keys.ID)) {
            user_id = sharedpreferences.getString(Keys.ID, "");
        }
    }
    // initilization value
    private void initFieldListner() {
        buttonNext = (Button) findViewById(R.id.buttonNext);
        listViewBussinessCheckList = (ListView) findViewById(R.id.listViewBussinessCheckList);
        buttonNext.setOnClickListener(this);
    }

    // setting up toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.business_Checklist));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onClick(View v) {
        if(v==buttonNext){
            checkedArraylist = businessCheckListAdapter.getCheckedArrayList();

            JSONObject jsonObjectCheckListID=new JSONObject();
            for(int i= 0;i<checkedArraylist.size();i++){
                try {
                    jsonObjectCheckListID.put(i+"",checkedArraylist.get(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            JSONObject jsonObject=new JSONObject();
            try {
                jsonObject.put("user_id",user_id);
                jsonObject.put("checklist_id",jsonObjectCheckListID);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            submitCheckListData(jsonObject);
        }

    }

    private void getBusinessCheckListData(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(BusinessCheckList.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            WebServiceCall webServiceCall = new WebServiceCall(BusinessCheckList.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        if (!response.equals("")) {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean success = jsonObject.getBoolean("success");
                            if (success) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                DataParser dataParser = new DataParser();
                                bussinessCheckListArrayList = dataParser.parseBussinessCheckListData(jsonArray);

                                for (int i=0;i<bussinessCheckListArrayList.size();i++){
                                    String check_status=bussinessCheckListArrayList.get(i).getCheck_status();
                                    if(check_status.equals("1")){
                                        checkboxListStatus.add(i);
                                        checkedArrayListInitial.add(bussinessCheckListArrayList.get(i).getQuestion_id());
                                    }
                                }

                                businessCheckListAdapter = new BusinessCheckListAdapter(BusinessCheckList.this, bussinessCheckListArrayList,checkboxListStatus,checkedArrayListInitial);
                                listViewBussinessCheckList.setAdapter(businessCheckListAdapter);

                            }

                        } else {
                            Toast.makeText(BusinessCheckList.this, "Something went wrong.Try Again.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onError(String error) {

                    progressDialog.dismiss();
                    Toast.makeText(BusinessCheckList.this, error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject,ServiceUrls.ALL_BUSSINESS_CHECKLIST_URL);
        } else {
            Toast.makeText(BusinessCheckList.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void submitCheckListData(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(BusinessCheckList.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(BusinessCheckList.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        String message = jsonObjectResponse.getString("message");
                        if (success) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.ADD_BUSSINESS_CHECKLIST_URL);
            progressDialog.show();
        } else {
            Toast.makeText(BusinessCheckList.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }
}
