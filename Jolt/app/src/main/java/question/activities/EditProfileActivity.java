package question.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.BuildConfig;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.MainActivity;
import com.jolt.R;
import com.squareup.picasso.Picasso;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import permissions.MarshmallowPermission;
import permissions.PermissionKeys;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.INetworkResponse;
import utils.Keys;
import utils.MultipartWebServiceCall;
import utils.ServiceUrls;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int CAMERA_REQUEST = 10;
    private EditText nameEditProfile, mobileEditProfile, emailEditProfile, cityEditProfile, graduationEditProfile,
            ageEditProfile, passwordEditProfile, confirmPasswordEditProfile;
    private SharedPreferences sharedPreferences;
    private String userName, userEmail, userMobile;
    private String userId;
    private Button submitEditProfileButton;
    private ImageView imageViewEditProfile;
    private ConnectionDetector connectionDetector;
    MarshmallowPermission marshmallowPermission;
    private Uri imgUri;
    private String imagePath;
    private MultipartEntityBuilder multipartEntityBuilder;
    private FileBody file;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setupToolbar();
        connectionDetector = new ConnectionDetector(this);
        marshmallowPermission = new MarshmallowPermission(this);

        initialization();
        getSharedPreferenceData();
        getBundleData();
    }

    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String getUserName = bundle.getString(Keys.NAME);
            String getEmail = bundle.getString(Keys.EMAIL);
            String getMobile = bundle.getString(Keys.MOBILE);
            String getCity = bundle.getString(Keys.CITY);
            String getGraduation = bundle.getString(Keys.GRADUATION);
            String getAge = bundle.getString(Keys.AGE);
            String getPhoto = bundle.getString(Keys.PHOTO);

            nameEditProfile.setText(getUserName);
            mobileEditProfile.setText(getMobile);
            emailEditProfile.setText(getEmail);
            cityEditProfile.setText(getCity);
            ageEditProfile.setText(getAge);
            graduationEditProfile.setText(getGraduation);
            if (getPhoto != null) {
                imageViewEditProfile.setVisibility(View.VISIBLE);
                Picasso.with(getApplicationContext()).load(getPhoto).placeholder(R.drawable.thumbnail).into(imageViewEditProfile);
            }
        }
    }

    private void getSharedPreferenceData() {
        sharedPreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, MODE_PRIVATE);
        if (sharedPreferences.contains(Keys.NAME)) {
            userId = sharedPreferences.getString(Keys.ID, null);
            // userName =sharedPreferences.getString(Keys.NAME,null);
            //userEmail = sharedPreferences.getString(Keys.EMAIL,null);
            // userMobile = sharedPreferences.getString(Keys.PHONE,null);
            // nameEditProfile.setText(userName);
            // emailEditProfile.setText(userEmail);
            // mobileEditProfile.setText(userMobile);
        }


    }

    private void initialization() {
        nameEditProfile = (EditText) findViewById(R.id.nameEditProfile);
        mobileEditProfile = (EditText) findViewById(R.id.mobileEditProfile);
        emailEditProfile = (EditText) findViewById(R.id.emailEditProfile);
        cityEditProfile = (EditText) findViewById(R.id.cityEditProfile);
        graduationEditProfile = (EditText) findViewById(R.id.graduationEditProfile);
        ageEditProfile = (EditText) findViewById(R.id.ageEditProfile);
        passwordEditProfile = (EditText) findViewById(R.id.passwordEditProfile);
        confirmPasswordEditProfile = (EditText) findViewById(R.id.confirmPasswordEditProfile);
        submitEditProfileButton = (Button) findViewById(R.id.submitEditProfileButton);
        submitEditProfileButton.setOnClickListener(this);
        imageViewEditProfile = (ImageView) findViewById(R.id.imageViewEditProfile);

        imageViewEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] permissions = {PermissionKeys.PERMISSION_CAMERA, PermissionKeys.PERMISSION_READ_EXTERNAL_STORAGE};
                if (marshmallowPermission.isPermissionGrantedAll(PermissionKeys.REQUEST_CODE_PERMISSION_ALL, permissions)) {
                    selectImage();
                }
            }
        });

    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Profile Image!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    captureImage();
                }
                if (which == 1) {
                    openGallery();
                }
            }
        });
        builder.show();
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoPickerIntent, 5);
    }

    private void captureImage() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        (new File(Environment.getExternalStorageDirectory()+"/jolt/")).mkdirs();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy-hhmmss");
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {*/

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }

            imgUri = FileProvider.getUriForFile(EditProfileActivity.this,
                    "com.example.android.fileprovider",
                    /*new File(Environment.getExternalStorageDirectory()+"/jolt/img" + sdf.format(new Date()) + ".jpg")*/photoFile);

            /*List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                grantUriPermission(packageName, imgUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }*/
            /*cameraIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cameraIntent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);*/
        /*} else {
            imgUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory()+"/jolt/img" + sdf.format(new Date()) + ".jpg"));
        }*/
        //imgUri = Uri.fromFile(new File("/sdcard/sbl/img" + sdf.format(new Date()) + ".jpg"));

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        imagePath = image.getAbsolutePath();
        return image;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                }else {
                    //imagePath = imgUri.getPath();
                }

                Picasso.with(EditProfileActivity.this)
                        .load(imgUri)
                        .error(R.drawable.basic_user)
                        .into(imageViewEditProfile);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(EditProfileActivity.this,
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(EditProfileActivity.this,
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == 5) {
            if (data != null && resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                imagePath = getRealPathFromURI(EditProfileActivity.this, selectedImage);
                Picasso.with(EditProfileActivity.this)
                        .load(selectedImage)
                        .error(R.drawable.basic_user)
                        .into(imageViewEditProfile);
            } else {
                Toast.makeText(EditProfileActivity.this,
                        "Sorry! Failed to take image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    // setting all the functionality of toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.edit_profile));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitEditProfileButton:

                String password = passwordEditProfile.getText().toString();
                String confirmPassword = confirmPasswordEditProfile.getText().toString();
                if (password.length() == 0) {
                    passwordEditProfile.setError("password can't be empty");
                } else if (!confirmPassword.equals(password)) {
                    Toast.makeText(this, "Confirm Password miss match", Toast.LENGTH_SHORT).show();
                } else {
                    sendData();
                }
                break;
        }
    }


    private void sendData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Keys.USERID, userId);
            jsonObject.put(Keys.NAME, nameEditProfile.getText().toString());
            jsonObject.put(Keys.EMAIL, emailEditProfile.getText().toString());
            jsonObject.put(Keys.MOBILE, mobileEditProfile.getText().toString());
            jsonObject.put(Keys.CITY, cityEditProfile.getText().toString());
            jsonObject.put(Keys.AGE, ageEditProfile.getText().toString());
            jsonObject.put(Keys.GRADUATION, graduationEditProfile.getText().toString());
            jsonObject.put(Keys.PASSWORD, passwordEditProfile.getText().toString());
          /*  if(checkBoxSparetyre.isChecked()){
                jsonObject.put("have_spare_tyre","1");
            }else {
                jsonObject.put("have_spare_tyre","0");
            }*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
        multipartEntityBuilder = MultipartEntityBuilder.create();
        multipartEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (imagePath != null) {
            file = new FileBody(new File(imagePath));
            multipartEntityBuilder.addPart("photo", file);
        }
        multipartEntityBuilder.addPart(Keys.DATA, new StringBody(jsonObject.toString(), ContentType.TEXT_PLAIN));
        // Service is passed in method sendUserRequestData for knowing which request is made by user.
        sendUserRequestData(multipartEntityBuilder, "Service");

    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    private void sendUserRequestData(MultipartEntityBuilder multipartEntityBuilder, final String status) {
        final ProgressDialog progressDialog = new ProgressDialog(EditProfileActivity.this, R.style.MyTheme);
        progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        MultipartWebServiceCall multipartWebServiceCall = new MultipartWebServiceCall(EditProfileActivity.this, new INetworkResponse() {
            @Override
            public void onSuccess(String response) {
                JSONObject jsonObject1 = null;
                try {
                    jsonObject1 = new JSONObject(response);
                    String msg = jsonObject1.getString("message");
                    boolean success = jsonObject1.getBoolean("success");
                    if (success) {
                        Toast.makeText(EditProfileActivity.this, "" + msg, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(EditProfileActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    if (progressDialog.isShowing() || progressDialog != null) {
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(EditProfileActivity.this, error, Toast.LENGTH_SHORT).show();
                if (progressDialog.isShowing() || progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        });
        multipartWebServiceCall.execute(multipartEntityBuilder, ServiceUrls.EDIT_PROFILE_URL);

        progressDialog.show();
    }
}
