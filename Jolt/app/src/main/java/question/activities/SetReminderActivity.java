package question.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import jolt.registration.LoginActivity;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.WebServiceCall;

public class SetReminderActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener{
    private TextView textViewNumberOfDays;
    private Button buttonSetReminder;
    private Spinner spinnerNumberOfDays;
    private String numberOfDays="",user_id;
    private ConnectionDetector connectionDetector;
    private SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_reminder);
        connectionDetector=new ConnectionDetector(SetReminderActivity.this);
        setupToolbar();
        initializer();
        initListeners();
        getUserData();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Set Reminder");
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up
        return false;
    }

    private void getUserData() {
        sharedpreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Keys.ID)) {
            user_id = sharedpreferences.getString(Keys.ID, "");
        }
    }

    private void initializer() {
        buttonSetReminder=(Button)findViewById(R.id.buttonSetReminder);
        //textViewNumberOfDays=(TextView)findViewById(R.id.textViewNumberOfDays);

        spinnerNumberOfDays = (Spinner) findViewById(R.id.spinnerNumberOfDays);
        // Spinner click listener
        spinnerNumberOfDays.setOnItemSelectedListener(this);
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("1");
        categories.add("2");
        categories.add("3");categories.add("4");categories.add("5");categories.add("6");categories.add("7");categories.add("8");categories.add("9");categories.add("10");
        categories.add("11");categories.add("12");categories.add("13");categories.add("14");categories.add("15");
        categories.add("16");categories.add("17");categories.add("18");categories.add("19");categories.add("20");categories.add("21");
        categories.add("22");categories.add("23");categories.add("24");categories.add("25");categories.add("26");categories.add("27");
        categories.add("28");categories.add("29");categories.add("30");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinnerNumberOfDays.setAdapter(dataAdapter);
    }

    private void initListeners() {
        buttonSetReminder.setOnClickListener(this);
       // textViewNumberOfDays.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==buttonSetReminder){
            if(!numberOfDays.equals("")){
                JSONObject jsonObject=new JSONObject();
                try {
                    jsonObject.put("user_id",user_id);
                    jsonObject.put("days",numberOfDays);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                submitData(jsonObject);
            }

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.spinnerNumberOfDays:
                numberOfDays = adapterView.getSelectedItem().toString();
                //Toast.makeText(SpinnerTest.this, year, Toast.LENGTH_LONG).show();
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void submitData(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(SetReminderActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(SetReminderActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        Log.d(this.getClass().getName(),"LOGIN_RESPONSE" + response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        String message = jsonObjectResponse.getString("message");
                        if (success) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            finish();

                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.SET_REMINDERS);
            progressDialog.show();
        } else {
            Toast.makeText(SetReminderActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }
}
