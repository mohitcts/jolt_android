package question.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONException;
import org.json.JSONObject;

import jolt.registration.LoginActivity;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.Utils;
import utils.WebServiceCall;

public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText editTextUserName,editTextEmail,editTextComment;
    private Button buttonSubmit;
    private String username,email,comment,user_id;
    private ConnectionDetector connectionDetector;
    private SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        connectionDetector=new ConnectionDetector(FeedbackActivity.this);
        setupToolbar();
        initializer();
        initListeners();
        getUserData();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Give feedback");
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up
        return false;
    }

    private void getUserData() {
        sharedpreferences = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Keys.ID)) {
            user_id = sharedpreferences.getString(Keys.ID, "");
        }
    }
    private void initListeners() {
        buttonSubmit.setOnClickListener(this);
    }

    private void initializer() {
        editTextUserName=(EditText)findViewById(R.id.editTextUserName);
        editTextEmail=(EditText)findViewById(R.id.editTextEmail);
        editTextComment=(EditText)findViewById(R.id.editTextComment);
        buttonSubmit=(Button) findViewById(R.id.buttonSubmit);
    }

    @Override
    public void onClick(View v) {
        if(v==buttonSubmit){
            username=editTextUserName.getText().toString();
            email=editTextEmail.getText().toString();
            comment=editTextComment.getText().toString();
            if(username.length()==0){
                editTextUserName.requestFocus();
                Toast.makeText(FeedbackActivity.this, "Please Enter User Name", Toast.LENGTH_SHORT).show();
            }else if(email.length()==0){
                editTextEmail.requestFocus();
                Toast.makeText(FeedbackActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
            }else if (!Utils.isValidEmail(email)) {
                editTextEmail.setText("");
                editTextEmail.setHintTextColor(Color.RED);
                editTextEmail.setError("Please enter a valid email.");
                Toast.makeText(FeedbackActivity.this, "PLEASE ENTER A VALID EMAIL", Toast.LENGTH_SHORT).show();
            } else if(comment.length()==0){
                editTextComment.requestFocus();
                Toast.makeText(FeedbackActivity.this, "Please Enter Comment", Toast.LENGTH_SHORT).show();
            }else {
                JSONObject jsonObject=new JSONObject();
                try {
                    jsonObject.put("user_id",user_id);
                    jsonObject.put("name",username);
                    jsonObject.put("email",email);
                    jsonObject.put("message",comment);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                submitData(jsonObject);
            }
        }
    }

    private void submitData(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(FeedbackActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(FeedbackActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        Log.d(this.getClass().getName(),"LOGIN_RESPONSE" + response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        String message = jsonObjectResponse.getString("message");
                        if (success) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            finish();

                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.FEEDBACK_URL);
            progressDialog.show();
        } else {
            Toast.makeText(FeedbackActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }
}
