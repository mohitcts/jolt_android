package question.activities;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;
import com.jolt.SplashScreens;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import jolt.registration.LoginActivity;
import jolt.registration.TermsConditionActivity;
import permissions.MarshmallowPermission;
import permissions.PermissionKeys;
import utils.AppConstants;
import utils.Downloader;
import utils.DownloaderPaid;

public class PaidAssessmentActivity extends AppCompatActivity implements View.OnClickListener{

    private WebView webView;
    private ProgressDialog progressDialog;
    private Bundle bundle;
    private String pdf,value,from;
    private TextView textViewToolbar;
    private ImageView imageViewDownload;
    private ImageView imageViewBack;
    private Handler handler;
    private Runnable runnable;
    private MarshmallowPermission marshmallowPermission;
    private File fileURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paid_assessment);
        marshmallowPermission=new MarshmallowPermission(PaidAssessmentActivity.this);
        initializer();
        initListeners();
        ////setupToolbar();
        getBundleValues();
        setValues();
        setWebView();
    }

    private void initListeners() {
        imageViewBack.setOnClickListener(this);
        imageViewDownload.setOnClickListener(this);
    }

    private void initializer() {
        webView=(WebView)findViewById(R.id.webView);
        imageViewBack=(ImageView)findViewById(R.id.imageViewBack);
        imageViewDownload=(ImageView)findViewById(R.id.imageViewDownload);
        textViewToolbar=(TextView)findViewById(R.id.textViewToolbar);
    }

    private void getBundleValues() {
        bundle=getIntent().getExtras();
        if(bundle!=null){
            pdf=bundle.getString("pdf","");
            value=bundle.getString("value","");
            /*from=bundle.getString("from","");
            if(from.equals("buy")){
                textViewBuy.setVisibility(View.VISIBLE);
            }*/
        }
    }

    private void setValues() {
       textViewToolbar.setText(value);
    }

    private void setWebView() {
        progressDialog = new ProgressDialog(PaidAssessmentActivity.this, R.style.MyTheme);
        progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(false);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdf);
       // webView.loadDataWithBaseURL("", description, "text/html", "UTF-8", "");
        webView.setWebViewClient(new AppWebViewClients());
    }

    @Override
    public void onClick(View v) {
        if(v==imageViewBack){
            finish();
        }
        if(v==imageViewDownload){
            final AlertDialog.Builder builder = new AlertDialog.Builder(PaidAssessmentActivity.this, R.style.AppCompatAlertDialogStyle);
            builder.setTitle("Do you want to Download?");
            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    String[] permissions = {PermissionKeys.PERMISSION_WRITE_EXTERNAL_STORAGE, PermissionKeys.PERMISSION_READ_EXTERNAL_STORAGE};
                    if (marshmallowPermission.isPermissionGrantedAll(PermissionKeys.REQUEST_CODE_PERMISSION_ALL, permissions)) {
                        downloadPdfFile(pdf);
                    }
                }
            });
            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.show();

        }
    }

    public class AppWebViewClients extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            };
            handler.postDelayed(runnable, 3000);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);

    }

    private File createFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".pdf",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //imagePath = image.getAbsolutePath();
        return image;
    }

    private void downloadPdfFile(String urlPdf) {
        try {
            fileURI=createFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        AppConstants.uriDoc = FileProvider.getUriForFile(PaidAssessmentActivity.this,
                "com.example.android.fileprovider",
                fileURI);

        String extStorageDirectory = Environment.getExternalStorageDirectory()
                .toString();
        File folder = new File(extStorageDirectory, "Jolt");
        if (!folder.exists()) {
            folder.mkdir();
        }
        String fileName = urlPdf.substring(urlPdf.lastIndexOf('/') + 1);
        File file = new File(folder, fileName);
        try {
            if (!file.exists()) {
                file.createNewFile();
                new DownloaderPaid(PaidAssessmentActivity.this, urlPdf, file,"pdf","Mini Business Plan").execute();
            } else {

                Toast.makeText(PaidAssessmentActivity.this, "Downloaded and saved in your device inside JOLT folder.", Toast.LENGTH_SHORT).show();
                //Toast.makeText(PaidAssessmentActivity.this, "", Toast.LENGTH_SHORT).show();
                /*File file2 = new File(Environment.getExternalStorageDirectory() + "/Jolt/" + fileName);
                PackageManager packageManager = getApplicationContext().getPackageManager();

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                Uri uri = Uri.fromFile(file);
                intent.setDataAndType(AppConstants.uriDoc, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                try {
                    startActivity(intent);
                }
                catch (ActivityNotFoundException e) {
                    Toast.makeText(PaidAssessmentActivity.this, "No Pdf Reader found in your device. Please install.", Toast.LENGTH_SHORT).show();
                }*/

            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
