package question.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.jolt.R;

import java.io.File;

import utils.AppConstants;

public class MiniPDfViewActivity extends AppCompatActivity implements View.OnClickListener{
    private WebView webViewPdfView;
    private ProgressDialog progressDialog;
    private String url,pdf_url,type;
    private ImageView imageViewBack,imageViewShare;
    private TextView textViewToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mini_pdf_view);
        initializer();
        initListeners();
        getBundleData();
        setWebView();
    }

    private void initializer() {
        webViewPdfView=(WebView)findViewById(R.id.webViewPdfView);
        imageViewShare=(ImageView)findViewById(R.id.imageViewShare);
        imageViewBack=(ImageView)findViewById(R.id.imageViewBack);
        textViewToolbar=(TextView)findViewById(R.id.textViewToolbar);
    }

    private void initListeners() {
        imageViewBack.setOnClickListener(this);
        imageViewShare.setOnClickListener(this);
    }

    private void getBundleData() {
        Bundle bundle = new Bundle();
        bundle = getIntent().getExtras();
        if (bundle != null) {
            pdf_url = bundle.getString("url", "");
            type = bundle.getString("type", "");
        }
        textViewToolbar.setText(type);
    }

    private void setWebView() {
        progressDialog = new ProgressDialog(MiniPDfViewActivity.this, R.style.MyTheme);
        progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        url="http://drive.google.com/viewerng/viewer?embedded=true&url=" +pdf_url;
        webViewPdfView.getSettings().setJavaScriptEnabled(true);
        webViewPdfView.getSettings().setBuiltInZoomControls(true);
        webViewPdfView.getSettings().setSupportZoom(true);
        webViewPdfView.loadUrl(url);
        webViewPdfView.setWebViewClient(new myWebClient());
    }

    @Override
    public void onClick(View v) {
        if(v==imageViewBack){
            finish();
        }
        if(v==imageViewShare){
            String fileName = pdf_url.substring(pdf_url.lastIndexOf('/') + 1);
            File file2 = new File(Environment.getExternalStorageDirectory() + "/Jolt/" + fileName);

           /* Uri imgUri = FileProvider.getUriForFile(MiniPDfViewActivity.this,
                    "com.example.android.fileprovider",
                    file2);*/

           /* Intent shareIntent = ShareCompat.IntentBuilder.from(MiniPDfViewActivity.this)
                    .setStream(AppConstants.uriDoc)
                    .getIntent();
// Provide read access
            shareIntent.setData(AppConstants.uriDoc);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);*/

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_STREAM, AppConstants.uriDoc);
            startActivity(sendIntent.createChooser(sendIntent,"send"));
        }
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            // webViewSpecificationView = view;
            progressDialog.dismiss();
        }
    }
}
