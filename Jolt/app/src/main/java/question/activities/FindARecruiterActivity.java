package question.activities;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.CareerPlanningAdapter;
import adapters.FindARecruiterAdapter;
import projections.ItemFindARecruiter;
import utils.ConnectionDetector;
import utils.DataParser;
import utils.INetworkResponse;
import utils.ServiceUrls;
import utils.WebServiceCall;

public class FindARecruiterActivity extends AppCompatActivity {
    private ListView listViewFindARecriter;
    private ConnectionDetector connectionDetector;
    private ArrayList<ItemFindARecruiter> findARecruiterArrayList;
    private FindARecruiterAdapter findArecruiterAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_arecruiter);
        connectionDetector = new ConnectionDetector(FindARecruiterActivity.this);
        setupToolbar();
        initializer();
        getRecruiterData();
    }

    private void getRecruiterData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        getData(jsonObject);
    }


    // setting up toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.find_recruiter));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    private void initializer() {
        listViewFindARecriter = (ListView) findViewById(R.id.listViewFindARecriter);
    }

    private void getData(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(FindARecruiterActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(FindARecruiterActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        Log.d(this.getClass().getName(), "LOGIN_RESPONSE" + response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        //String message = jsonObjectResponse.getString("message");
                        if (success) {
                            JSONArray jsonArray = jsonObjectResponse.getJSONArray("data");
                            DataParser dataParser = new DataParser();
                            findARecruiterArrayList = dataParser.parseFindARecruiterData(jsonArray);
                            findArecruiterAdapter = new FindARecruiterAdapter(FindARecruiterActivity.this, findARecruiterArrayList);
                            listViewFindARecriter.setAdapter(findArecruiterAdapter);

                        } else {
                            Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
                        }
                    } catch (
                            JSONException e
                            )

                    {
                        e.printStackTrace();
                    } finally

                    {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.FIND_A_RECRUITER_URL);
            progressDialog.show();
        } else

        {
            Toast.makeText(FindARecruiterActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }


}
