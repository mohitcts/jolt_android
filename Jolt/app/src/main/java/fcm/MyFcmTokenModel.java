package fcm;

import android.content.Context;
import android.content.SharedPreferences;

import utils.Keys;


/**
 * Created by android1 on 7/12/16.
 */

public class MyFcmTokenModel {

    SharedPreferences sharedPreferences;

    public MyFcmTokenModel(Context context) {
        sharedPreferences=context.getSharedPreferences(Keys.PREF_FCM_TOKEN,Context.MODE_PRIVATE);
    }

    public String getToken() {
        return sharedPreferences.getString(Keys.FCM_TOKEN,"");
    }

    public void setToken(String token) {
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(Keys.FCM_TOKEN,token);
        editor.commit();
    }

}