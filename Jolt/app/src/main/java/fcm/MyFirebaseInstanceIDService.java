package fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by android1 on 7/12/16.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseInstanceIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log.v("token",refreshedToken.toString());
        //Logger.d(TAG +" Refreshed token: " + refreshedToken);
        saveTokenPref(refreshedToken);
    }

    private void saveTokenPref(String refreshedToken) {
        MyFcmTokenModel myFcmTokenModel = new MyFcmTokenModel(getApplicationContext());
        myFcmTokenModel.setToken(refreshedToken);
    }

}
