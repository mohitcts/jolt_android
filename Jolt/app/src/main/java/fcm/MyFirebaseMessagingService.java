package fcm;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.jolt.MainActivity;
import com.jolt.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import jolt.registration.LoginActivity;
import utils.AppConstants;
import utils.Keys;

/**
 * Created by android1 on 7/12/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMessagingService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        //Logger.d(TAG+ " From: " + remoteMessage.getFrom());

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            //Logger.d(TAG+ " Message Notification Body: " + remoteMessage.getNotification().getBody());
            //sendNotification(remoteMessage.getNotification().getBody());
           // handleNotification(remoteMessage.getNotification().getBody()+"");
            //Log.e("BODY--->",remoteMessage.getNotification().getBodyLocalizationArgs()+"");
            Log.e("BODY--->",remoteMessage.getNotification().getBody()+"");
            /*Log.e("Click Action:" , remoteMessage.getNotification().getClickAction()+"");*/
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            //Logger.d(TAG+ " Message data payload: " + remoteMessage.getData());
            Log.e("DATA--->", remoteMessage.getData().toString());
            //String messagee = remoteMessage.getData().toString();

            JSONObject json = null;
            JSONObject object = null;
            String message = "";
            try {
                json = new JSONObject(remoteMessage.getData().toString());
                Log.e("JSON", json.toString());
                notificationManage(json.toString());
                object = json.getJSONObject("message");
                // message=object.getString(KeysCab.NOTIFICATION_TYPE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void notificationManage(String remoteMessage) {
        String notification_type = "";
        try {
            JSONObject object = new JSONObject(remoteMessage);

            AppConstants.isFromNotification=true;
            //It should be notification type
            notification_type = object.getString("notification_type");
            if (notification_type.equals("Career Plan")) {
                getSharedPreferences(AppConstants.NOTIFICATION_PREFERENCE, 0).edit().putString(Keys.CAREER_PLAN, String.valueOf(object)).apply();
                AppConstants.isCareerPlan = true;
            }else if (notification_type.equals("Mini Business Plan")) {
                getSharedPreferences(AppConstants.NOTIFICATION_PREFERENCE, 0).edit().putString(Keys.MINI_BUSINESS_PLAN, String.valueOf(object)).apply();
                AppConstants.isMinibusinessPlan = true;
            }else if (notification_type.equals("Business Checklist")) {
                getSharedPreferences(AppConstants.NOTIFICATION_PREFERENCE, 0).edit().putString(Keys.BUSINESS_CHECKLIST, String.valueOf(object)).apply();
                AppConstants.isBusinessChecklist = true;
            }else if (notification_type.equals("Business Checklist Quarter")) {
                getSharedPreferences(AppConstants.NOTIFICATION_PREFERENCE, 0).edit().putString(Keys.BUSINESS_CHECKLIST_QUARTER, String.valueOf(object)).apply();
                AppConstants.isBusinessChecklistQuarter = true;
            }else if (notification_type.equals("New Employee")) {
                getSharedPreferences(AppConstants.NOTIFICATION_PREFERENCE, 0).edit().putString(Keys.NEW_EMPLOYEE, String.valueOf(object)).apply();
                AppConstants.isNewEmployee = true;
            }else if (notification_type.equals("Current Employee")) {
                getSharedPreferences(AppConstants.NOTIFICATION_PREFERENCE, 0).edit().putString(Keys.EMPLOYEE, String.valueOf(object)).apply();
                AppConstants.isEmployee = true;
            }else if (notification_type.equals("Employment")) {
                getSharedPreferences(AppConstants.NOTIFICATION_PREFERENCE, 0).edit().putString(Keys.EMPLOYMENT, String.valueOf(object)).apply();
                AppConstants.isEmployment = true;
            }else if (notification_type.equals("Leader")) {
                getSharedPreferences(AppConstants.NOTIFICATION_PREFERENCE, 0).edit().putString(Keys.LEADER, String.valueOf(object)).apply();
                AppConstants.isLeader = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
        sendNotification(notification_type, remoteMessage);
    }

    private void sendNotification(String notification_type, String message) {
        if (AppConstants.isMainActivityPageOpen) {
            if (!getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, 0).getString(Keys.EMAIL, "").equals("")) {
                Intent intent = new Intent(this, MainActivity.class);
                //intent.putExtra("NOTIFICATION_CONTENT", message);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(500);
                this.startActivity(intent);
            } else {
                Intent intent = new Intent(this, LoginActivity.class);
               // intent.putExtra("NOTIFICATION_CONTENT", message);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(500);
                this.startActivity(intent);
            }
        } else {
            AppConstants.isFromNotification = true;
            if (!getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, 0).getString(Keys.EMAIL, "").equals("")) {
                Intent intent = new Intent(this, MainActivity.class);
                // intent.putExtra("NOTIFICATION_CONTENT", message);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(500);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setSmallIcon(R.mipmap.ic_launcher).setTicker(getResources().getString(R.string.app_name)).setWhen(0)
                       /* .setSmallIcon(R.mipmap.ic_launcher)*/

                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText(notification_type)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(0, notificationBuilder.build());

            } else {
                Intent intent = new Intent(this, LoginActivity.class);
                //intent.putExtra("NOTIFICATION_CONTENT", message);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(500);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setSmallIcon(R.mipmap.ic_launcher).setTicker(getResources().getString(R.string.app_name)).setWhen(0)
                        /*.setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))*/
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText(notification_type)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(0, notificationBuilder.build());
            }
        }
    }

    private void handleNotification(String message) {
        if (isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            notificationManage(message);
            Toast.makeText(getApplicationContext(),"Background Call "+message,Toast.LENGTH_LONG).show();
            // Log.e(TAG, "Data Background: " + message.toString());
        }
    }


    private  boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }
        return isInBackground;
    }
}
