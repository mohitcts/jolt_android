package fcm;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.ResourceBundle;

import jolt.registration.LoginActivity;
import utils.AppConstants;
import utils.Keys;

/**
 * Created by rishabh on 5/18/2017.
 */

public class DeleteTokenService extends IntentService {

    public static final String TAG = DeleteTokenService.class.getSimpleName();
    private SharedPreferences sharedPreferences;

    public DeleteTokenService()
    {
        super(TAG);
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        try
        {
            // Check for current token
            String originalToken = getToken();
            Log.e(TAG, "Token before deletion: " + originalToken);

            // Resets Instance ID and revokes all tokens.
            FirebaseInstanceId.getInstance().deleteInstanceId();

            // Clear current saved token
            //saveTokenToPrefs("");

            // Check for success of empty token
           // String tokenCheck = getTokenFromPrefs();
           // Log.d(TAG, "Token deleted. Proof: " + tokenCheck);

            // Now manually call onTokenRefresh()
            //Log.d(TAG, "Getting new token");
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.e(TAG, "Token after deletion: " + refreshedToken);
            //saveTokenPref(refreshedToken);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public String getToken() {
        sharedPreferences=getSharedPreferences(Keys.PREF_FCM_TOKEN, Context.MODE_PRIVATE);
        return sharedPreferences.getString(Keys.FCM_TOKEN,"");
    }

    /*private void saveTokenPref(String refreshedToken) {
        MyFcmTokenModel myFcmTokenModel = new MyFcmTokenModel(getApplicationContext());
        myFcmTokenModel.setToken(refreshedToken);
        logout();
    }

    private void logout() {
        getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, MODE_PRIVATE).edit().clear().commit();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }*/
}
