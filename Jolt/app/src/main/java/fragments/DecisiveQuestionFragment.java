package fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.MainActivity;
import com.jolt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import projections.ItemDecisiveQuestions;
import question.activities.DecisiveActivity;
import question.activities.ReactiveActivity;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.DataParser;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.WebServiceCall;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;


public class DecisiveQuestionFragment extends Fragment implements View.OnClickListener {


    private View rootView;
    private TextView textViewQuestion1, textViewQuestion2;
    private RadioGroup radioGroupQuestion1, radioGroupQuestion2;
    private RadioButton radioButtonQuestion1One, radioButtonQuestion1Two, radioButtonQuestion1Three, radioButtonQuestion1Four, radioButtonQuestion2One, radioButtonQuestion2Two, radioButtonQuestion2Three, radioButtonQuestion2Four;
    private Button buttonNext;
    private SharedPreferences sharedpreferences;
    private String userid;
    private ConnectionDetector connectionDetector;
    private ArrayList<ItemDecisiveQuestions> decisiveQuestionArrayList;
    private Bundle bundle;
    private String questionId1, questionId2;
    private String answer2;
    private LinearLayout linearLayoutQuestion2;
    private Context context;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_decisive_question, container, false);
        connectionDetector = new ConnectionDetector(getActivity());
        initializer();
        initListeners();
        getUserData();
        getBundleData();
        return rootView;
    }

    private void getBundleData() {
        bundle = getArguments();

       // page = bundle.getInt("page");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", userid);
            jsonObject.put("category", "DECISIVE");
            jsonObject.put("page", String.valueOf(((DecisiveActivity)getActivity()).page));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        getDecisiveQuestions(jsonObject);
    }

    private void getUserData() {
        sharedpreferences = getActivity().getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, MODE_PRIVATE);
        userid = sharedpreferences.getString(Keys.ID, "");
    }

    private void initializer() {
        buttonNext = (Button) rootView.findViewById(R.id.buttonNext);

        textViewQuestion1 = (TextView) rootView.findViewById(R.id.textViewQuestion1);
        textViewQuestion2 = (TextView) rootView.findViewById(R.id.textViewQuestion2);

        radioGroupQuestion1 = (RadioGroup) rootView.findViewById(R.id.radioGroupQuestion1);
        //radioGroupQuestion2 = (RadioGroup) rootView.findViewById(R.id.radioGroupQuestion2);

        /*radioButtonQuestion1One = (RadioButton) rootView.findViewById(R.id.radioButtonQuestion1One);
        radioButtonQuestion1Two = (RadioButton) rootView.findViewById(R.id.radioButtonQuestion1Two);
        radioButtonQuestion1Three = (RadioButton) rootView.findViewById(R.id.radioButtonQuestion1Three);
        radioButtonQuestion1Four = (RadioButton) rootView.findViewById(R.id.radioButtonQuestion1Four);
        radioButtonQuestion2One = (RadioButton) rootView.findViewById(R.id.radioButtonQuestion2One);
        radioButtonQuestion2Two = (RadioButton) rootView.findViewById(R.id.radioButtonQuestion2Two);
        radioButtonQuestion2Three = (RadioButton) rootView.findViewById(R.id.radioButtonQuestion2Three);
        radioButtonQuestion2Four = (RadioButton) rootView.findViewById(R.id.radioButtonQuestion2Four);*/

        //linearLayoutQuestion2=(LinearLayout)rootView.findViewById(R.id.linearLayoutQuestion2);
    }

    private void initListeners() {
        buttonNext.setOnClickListener(this);
    }

    private void getDecisiveQuestions(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(getActivity(), new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        if (success) {
                            JSONArray jsonArray = jsonObjectResponse.getJSONArray(Keys.DATA);
                            if (jsonArray.length() > 0) {
                                DataParser dataParser = new DataParser();
                                decisiveQuestionArrayList = new ArrayList<ItemDecisiveQuestions>();
                                decisiveQuestionArrayList = dataParser.parseDecesiveQuestions(jsonArray);
                                Log.e("SIZE____>",decisiveQuestionArrayList.size()+"");

                                for (int i = 0; i < decisiveQuestionArrayList.size(); i++) {
                                    if (i == 0) {
                                        String question1 = decisiveQuestionArrayList.get(0).getQuestion();
                                        String question2 = decisiveQuestionArrayList.get(0).getQuestion2();
                                        questionId1 = decisiveQuestionArrayList.get(0).getId();
                                        textViewQuestion2.setText(question2);
                                        textViewQuestion1.setText(question1);
                                        /*if(decisiveQuestionArrayList.size()<2){
                                            linearLayoutQuestion2.setVisibility(View.GONE);
                                        }*/
                                    } /*else if (decisiveQuestionArrayList.size() > 1) {
                                        String question = decisiveQuestionArrayList.get(1).getQuestion();
                                        questionId2 = decisiveQuestionArrayList.get(1).getId();
                                        textViewQuestion2.setText(question);
                                    }*/
                                }
                            } else {
                                getActivity().getSupportFragmentManager().popBackStack();
                                FragmentManager fm1 = getActivity().getSupportFragmentManager();
                                if (fm1.getBackStackEntryCount() > 1) {
                                    ((DecisiveActivity)getActivity()).page--;
                                }
                                collectAnswersAndPost();
                                //startActivity(new Intent(getActivity(), ReactiveActivity.class));
                            }
                        } else {
                            //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
                            getActivity().getSupportFragmentManager().popBackStack();
                            FragmentManager fm1 = getActivity().getSupportFragmentManager();
                            if (fm1.getBackStackEntryCount() > 1) {
                                ((DecisiveActivity)getActivity()).page--;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    getActivity().getSupportFragmentManager().popBackStack();
                    FragmentManager fm1 = getActivity().getSupportFragmentManager();
                    int a = fm1.getBackStackEntryCount();
                    if (fm1.getBackStackEntryCount() > 1) {
                        ((DecisiveActivity)getActivity()).page--;
                    }
                    progressDialog.dismiss();
                    // Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.QUESTIONS_URL);
            progressDialog.show();
        } else {
            Toast.makeText(getActivity(), "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void collectAnswersAndPost() {
        try {
            JSONObject jsonObjectQuestionId = new JSONObject();
            JSONObject jsonObjectRate = new JSONObject();

            JSONObject jsonObjectKeyAnswer = new JSONObject();

            for (int i = 0; i < ((DecisiveActivity) getActivity()).questionIdArrayList.size(); i++) {
                jsonObjectQuestionId.put(String.valueOf(i), ((DecisiveActivity) getActivity()).questionIdArrayList.get(i));
            }

            for (int i = 0; i < ((DecisiveActivity) getActivity()).rateArrayList.size(); i++) {
                jsonObjectRate.put(String.valueOf(i), ((DecisiveActivity) getActivity()).rateArrayList.get(i));
            }

            jsonObjectKeyAnswer.put("question_id", jsonObjectQuestionId);
            jsonObjectKeyAnswer.put("rate", jsonObjectRate);

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("user_id", userid);
            jsonObject1.put("key_answer", jsonObjectKeyAnswer);

            postAnswers(jsonObject1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;

    }

    @Override
    public void onClick(View v) {
        if (v == buttonNext) {

            String answer1 = ((RadioButton) rootView.findViewById(radioGroupQuestion1.getCheckedRadioButtonId())).getText().toString();
            ((DecisiveActivity)getActivity()).rateArrayList.add(answer1);
            /*if(decisiveQuestionArrayList.size() > 1) {
                answer2 = ((RadioButton) rootView.findViewById(radioGroupQuestion2.getCheckedRadioButtonId())).getText().toString();
            }*/

            ((DecisiveActivity)getActivity()).questionIdArrayList.add(questionId1);
            /*if(decisiveQuestionArrayList.size() > 1) {
                ((DecisiveActivity) getActivity()).questionIdArrayList.add(questionId2);
            }*/


            /*if(decisiveQuestionArrayList.size() > 1) {
                ((DecisiveActivity) getActivity()).rateArrayList.add(answer2);
            }*/

            Bundle bundle = new Bundle();
            ((DecisiveActivity)getActivity()).page++;
            //bundle.putInt("page", page);

            Fragment fragment = new DecisiveQuestionFragment();
            ((DecisiveActivity) getActivity()).addFragment(fragment, bundle);



            /*try {
                JSONObject jsonObjectQuestionId = new JSONObject();
                jsonObjectQuestionId.put("0", questionId1);
                if(decisiveQuestionArrayList.size() > 1) {
                    jsonObjectQuestionId.put("1", questionId2);
                }

                String answer1 = ((RadioButton) rootView.findViewById(radioGroupQuestion1.getCheckedRadioButtonId())).getText().toString();
                if(decisiveQuestionArrayList.size() > 1) {
                    answer2 = ((RadioButton) rootView.findViewById(radioGroupQuestion2.getCheckedRadioButtonId())).getText().toString();
                }

                JSONObject jsonObjectRate = new JSONObject();
                jsonObjectRate.put("0", answer1);
                if(decisiveQuestionArrayList.size() > 1) {
                    jsonObjectRate.put("1", answer2);
                }

                JSONObject jsonObjectKeyAnswer=new JSONObject();
                jsonObjectKeyAnswer.put("question_id",jsonObjectQuestionId);
                jsonObjectKeyAnswer.put("rate",jsonObjectRate);

                JSONObject jsonObject1=new JSONObject();
                jsonObject1.put("user_id",userid);
                jsonObject1.put("key_answer",jsonObjectKeyAnswer);

                postAnswers(jsonObject1);


            } catch (JSONException e) {
                e.printStackTrace();
            }*/
        }
    }

    private void postAnswers(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(getActivity(), new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        if (success) {
                            //JSONArray jsonArray = jsonObjectResponse.getJSONArray(Keys.DATA);
                            /*Bundle bundle = new Bundle();
                            page++;
                            bundle.putInt("page", page);

                            Fragment fragment = new DecisiveQuestionFragment();
                            ((DecisiveActivity) getActivity()).addFragment(fragment, bundle);*/
                            ReactiveActivity.page=0;
                            context.startActivity(new Intent(context, ReactiveActivity.class));

                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    // Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.RATE_URL);
            progressDialog.show();
        } else {
            Toast.makeText(getActivity(), "Network not available", Toast.LENGTH_SHORT).show();
        }
    }


}
