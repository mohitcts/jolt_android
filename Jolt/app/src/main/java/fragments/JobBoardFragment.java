package fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import adapters.JobBoardAdapter;
import projections.ItemCareerAdvice;
import question.activities.CareerPlanningWebviewActivity;
import question.activities.DecisiveActivity;
import question.activities.JobBoardActivity;
import utils.ConnectionDetector;
import utils.DataParser;
import utils.INetworkResponse;
import utils.ServiceUrls;
import utils.WebServiceGet;


public class JobBoardFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener{


    private View rootView;
    private ConnectionDetector connectionDetector;
    private ListView listViewJobBoard;
    private TextView textViewSearch;
    private boolean flag_loading;
    private int selection, count;
    private int mLastFirstVisibleItem;
    private ArrayList<ItemCareerAdvice> careerAdvice2ArrayList;
    private ArrayList<ItemCareerAdvice> careerAdviceArrayList;
    private JobBoardAdapter jobBoardAdapter;
    private String landing_page;
    private Dialog searchDialog;
    private String searchCategory;
    private String url = "";;
    private String search,city;
    private Bundle bundle;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_job_board, container, false);
        careerAdviceArrayList = new ArrayList();
        careerAdvice2ArrayList = new ArrayList<>();
        connectionDetector=new ConnectionDetector(getActivity());
        flag_loading = true;
        selection = 0;
        count = 0;
        initialization();
        initListeners();
        getBundleData();
        //getJobBoardData(count,selection);
        return rootView;
    }


    private void initialization() {
        listViewJobBoard =(ListView)rootView.findViewById(R.id.listViewJobBoard);
        textViewSearch=(TextView)rootView.findViewById(R.id.textViewSearch);
        listViewJobBoard.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (view.getId()== listViewJobBoard.getId()){
                    final  int currentFirstVisibleItem= listViewJobBoard.getFirstVisiblePosition();
                    mLastFirstVisibleItem= currentFirstVisibleItem;
                    //if ()
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (view.getId()== listViewJobBoard.getId()){

                    if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount !=0){

                        if (!flag_loading){
                            Log.e("OnScrollList","Scrolling..");
                            selection = firstVisibleItem + visibleItemCount-1;
                            count=count+1;
                            flag_loading=true;
                            getJobBoardData(count,selection);
                        }
                    }
                }
            }
        });
    }

    private void initListeners() {
        textViewSearch.setOnClickListener(this);
    }

    private void getJobBoardData(int pageCount, final int selection) {

        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            WebServiceGet webServiceGet = new WebServiceGet(getActivity(), new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        if (!response.equals("")) {
                            JSONObject jsonObject = new JSONObject(response);
                            int beforeSize = careerAdviceArrayList.size();
                            JSONArray jsonArrayCategories = jsonObject.getJSONArray("results");
                            if (jsonArrayCategories.length() > 0) {
                                DataParser dataParser = new DataParser();
                                careerAdvice2ArrayList = dataParser.parseJoBBoardData(jsonArrayCategories);
                                careerAdviceArrayList.addAll(careerAdvice2ArrayList);
                                int afterSize = careerAdviceArrayList.size();
                                if (beforeSize != afterSize) {
                                    jobBoardAdapter = new JobBoardAdapter(getActivity(), careerAdviceArrayList);
                                    listViewJobBoard.setAdapter(jobBoardAdapter);
                                    flag_loading = false;
                                    listViewJobBoard.setSelection(selection);
                                } else {
                                    flag_loading = true;
                                    listViewJobBoard.setSelection(selection);
                                }

                                listViewJobBoard.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        JSONObject jsonObjectRefs=careerAdviceArrayList.get(position).getRefs();
                                        try {
                                            landing_page=jsonObjectRefs.getString("landing_page");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Bundle bundle=new Bundle();
                                        bundle.putString("linkURL",landing_page);
                                        bundle.putString("value","Job");
                                        Intent intent=new Intent(getActivity(),CareerPlanningWebviewActivity.class);
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }
                                });
                            }
                        } else {
                            Toast.makeText(getActivity(), "Something went wrong.Try Again.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onError(String error) {
                    flag_loading = false;
                    count = count - 1;
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceGet.execute(ServiceUrls.JOB_BOARD_URL + count+url);
            Log.e("URL",ServiceUrls.JOB_BOARD_URL + count+url);
        } else {
            Toast.makeText(getActivity(), "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void getBundleData() {
        bundle = getArguments();
        url = bundle.getString("url");
        getJobBoardData(count,selection);
    }

    @Override
    public void onClick(View v) {
        if(v==textViewSearch){
            showSearchDialog();
        }
    }

    private void showSearchDialog() {
        searchDialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        searchDialog.setContentView(R.layout.layout_search);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        searchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //spinnerMakeTow = (Spinner) servicedialogTow.findViewById(R.id.spinnerMakeTow);

        ImageView imageViewBack = (ImageView) searchDialog.findViewById(R.id.imageViewBack);

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDialog.dismiss();
            }
        });

        final EditText editTextKeyword=(EditText)searchDialog.findViewById(R.id.editTextKeyword);
        final EditText editTextSearchCity=(EditText)searchDialog.findViewById(R.id.editTextSearchCity);

        Button buttonSearch=(Button)searchDialog.findViewById(R.id.buttonSearch);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                url="";

                search=editTextKeyword.getText().toString();
                city=editTextSearchCity.getText().toString();

                if(!search.equals("")){
                    url = url + "&search="+search;
                }
                if(!city.equals("")){
                    url = url + "&location="+city;
                }

                url = url+"&category="+searchCategory;

                Fragment fragment = new JobBoardFragment();
                Bundle bundle=new Bundle();
                bundle.putString("url",url);
                ((JobBoardActivity) getActivity()).addFragment(fragment,bundle);

                searchDialog.dismiss();

            }
        });

        Spinner spinnerCategory = (Spinner) searchDialog.findViewById(R.id.spinnerCategory);
        // Spinner click listener
        spinnerCategory.setOnItemSelectedListener(this);
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Account Management");
        categories.add("Creative & Design");
        categories.add("Data Science");
        categories.add("Education");
        categories.add("Finance");
        categories.add("Healthcare & Medicine");
        categories.add("Legal");
        categories.add("Operations");
        categories.add("Retail");
        categories.add("Social Media & Community");
        categories.add("Business & Strategy");
        categories.add("Customer Service");
        categories.add("Editorial");
        categories.add("Engineering");
        categories.add("Fundraising & Development");
        categories.add("HR & Recruiting");
        categories.add("Marketing & PR");
        categories.add("Project & Product Management");
        categories.add("Sales");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinnerCategory.setAdapter(dataAdapter);

        searchDialog.show();
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.spinnerCategory:
                searchCategory = adapterView.getSelectedItem().toString();
                searchCategory=searchCategory.replace(" ","+");
                searchCategory=searchCategory.replace("&","%26");
                //Toast.makeText(SpinnerTest.this, year, Toast.LENGTH_LONG).show();
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
