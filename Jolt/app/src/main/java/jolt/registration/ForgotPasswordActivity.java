package jolt.registration;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ConnectionDetector;
import utils.INetworkResponse;
import utils.ServiceUrls;
import utils.Utils;
import utils.WebServiceCall;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText editTextEmail;
    private Button buttonReset;
    private ConnectionDetector connectionDetector;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        connectionDetector = new ConnectionDetector(ForgotPasswordActivity.this);
        initializer();
        initListeners();
        setupToolbar();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Forgot Password");
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up
        return false;
    }

    private void initListeners() {
        buttonReset.setOnClickListener(this);
    }

    private void initializer() {
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        buttonReset = (Button) findViewById(R.id.buttonReset);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonReset) {
            email = editTextEmail.getText().toString();
            if (email.length() <= 0) {
                editTextEmail.setHintTextColor(Color.RED);
                editTextEmail.setError("Please Enter Your Email.");
            } else if (!Utils.isValidEmail(email)) {
                editTextEmail.setText("");
                editTextEmail.setHintTextColor(Color.RED);
                editTextEmail.setError("Please enter a valid email.");
                Toast.makeText(ForgotPasswordActivity.this, "PLEASE ENTER A VALID EMAIL", Toast.LENGTH_SHORT).show();
            } else {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("email", email);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                submitData(jsonObject);
            }

        }
    }

    private void submitData(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(ForgotPasswordActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            WebServiceCall webServiceCall = new WebServiceCall(ForgotPasswordActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        if (!response.equals("")) {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean success = jsonObject.getBoolean("success");
                            String message=jsonObject.getString("message");
                            if (success) {
                               // JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                Toast.makeText(ForgotPasswordActivity.this, ""+message, Toast.LENGTH_LONG).show();

                            }else {
                                Toast.makeText(ForgotPasswordActivity.this, ""+message, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, "Something went wrong.Try Again.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onError(String error) {

                    progressDialog.dismiss();
                    Toast.makeText(ForgotPasswordActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.FORGOT_PASSWORD_URl);
        } else {
            Toast.makeText(ForgotPasswordActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }
}
