package jolt.registration;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.jolt.AssesmentActivity;
import com.jolt.MainActivity;
import com.jolt.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import fcm.MyFcmTokenModel;
import fcm.MyFirebaseInstanceIDService;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.WebServiceCall;

public class RegistrationScreen extends AppCompatActivity implements View.OnClickListener {

    EditText editTextName,editTextEmail,editTextPassword,editTextConfirmPass;
    Button buttonRegister;
    CheckBox cbTermsCondition;
    TextView termsConditionText;
    private ConnectionDetector connectionDetector;
    private String fcmToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_screen);
        connectionDetector = new ConnectionDetector(this);

        setupToolbar();
        initFieldAndListner();
        getFcmKey();
    }


    private void initFieldAndListner(){
        editTextName=(EditText)findViewById(R.id.editTextName);
        editTextEmail=(EditText)findViewById(R.id.editTextEmail);
        editTextPassword=(EditText)findViewById(R.id.editTextPassword);
        editTextConfirmPass=(EditText)findViewById(R.id.editTextConfirmPass);
        buttonRegister=(Button)findViewById(R.id.buttonRegister);
        buttonRegister.setOnClickListener(this);
        cbTermsCondition=(CheckBox)findViewById(R.id.CBTermsConditions);
        termsConditionText=(TextView)findViewById(R.id.termsConditionText);
        termsConditionText.setOnClickListener(this);
    }

    private void getFcmKey() {
        MyFcmTokenModel myFcmTokenModel=new MyFcmTokenModel(getApplicationContext());
        fcmToken=myFcmTokenModel.getToken();
        Log.e("token",fcmToken.toString());
        //Toast.makeText(this, ""+token.toString(), Toast.LENGTH_SHORT).show();
    }


    // setting up toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Register");
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.ic_close_black_24dp);
        ab.setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.buttonRegister:

                if (cbTermsCondition.isChecked()){
                    /*try {
                        FirebaseInstanceId.getInstance().deleteInstanceId();
                        fcmToken = FirebaseInstanceId.getInstance().getToken();
                        Log.e("fcmToken--->",fcmToken);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/

                    String name = editTextName.getText().toString();
                    String email=editTextEmail.getText().toString();
                    String password=editTextPassword.getText().toString();

                     if(name.length()==0){
                         editTextName.setError("Please Enter Name");
                         editTextName.requestFocus();
                    }
                   else if(email.length()==0){
                        editTextEmail.setError("Please Enter Email");
                        editTextEmail.requestFocus();
                    }else if(password.length()==0){
                        editTextPassword.setError("Please Enter Password");
                        editTextPassword.requestFocus();
                    }

                    else {

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put(ServiceUrls.PARAMS.NAME,editTextName.getText().toString());
                            // jsonObject.put(ServiceUrls.PARAMS.USER_NAME, user_name);
                            jsonObject.put(ServiceUrls.PARAMS.EMAIL, editTextEmail.getText().toString());
                            jsonObject.put(ServiceUrls.PARAMS.PASSWORD, editTextPassword.getText().toString());
                            jsonObject.put("fcm_key", fcmToken);
                            jsonObject.put("device", "Android");
                            jsonObject.put("registration_type", "Normal");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(this.getClass().getName(), "JSON_OBJECT" + jsonObject);
                        getRegistration(jsonObject);
                    }
                }
                else {
                    Toast.makeText(this,"Please select terms and conditions",Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.termsConditionText:
                startActivity(new Intent(this,TermsConditionActivity.class));
                break;
        }
    }



    private void getRegistration(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(RegistrationScreen.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(RegistrationScreen.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        String message = jsonObjectResponse.getString("message");
                        if (success) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            JSONObject jsonObject = jsonObjectResponse.getJSONObject(Keys.DATA);
                            SharedPreferences.Editor editor = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, MODE_PRIVATE).edit();
                            editor.putString(Keys.EMAIL, jsonObject.getString(Keys.EMAIL));
                            editor.putString(Keys.NAME, jsonObject.getString(Keys.NAME));
                            editor.putString(Keys.ID,jsonObject.getString(Keys.ID));
                            editor.commit();
                          /*  JSONObject jsonObject = jsonObjectResponse.getJSONObject(Keys.DATA);
                            SharedPreferences.Editor editor = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, MODE_PRIVATE).edit();
                            editor.putString(Keys.EMAIL, jsonObject.getString(Keys.EMAIL));
                            editor.putString(Keys.NAME, jsonObject.getString(Keys.NAME));
                            editor.putString(Keys.USER_ID,jsonObject.getString(Keys.USER_ID));
                            editor.putString(Keys.PHONE,jsonObject.getString(Keys.PHONE));
                            editor.putString(Keys.STREET,jsonObject.getString(Keys.STREET));
                            editor.putString(Keys.CITY,jsonObject.getString(Keys.CITY));
                            editor.putString(Keys.STATE,jsonObject.getString(Keys.STATE));
                            editor.putString(Keys.ZIP,jsonObject.getString(Keys.ZIP));
                            editor.putString(Keys.VEHICLE_ID,jsonObject.getString(Keys.VEHICLE_ID));
                            editor.putString(Keys.VEHICLE_MAKE,jsonObject.getString(Keys.VEHICLE_MAKE));
                            editor.putString(Keys.VEHICLE_MODEL,jsonObject.getString(Keys.VEHICLE_MODEL));
                            editor.putString(Keys.VEHICLE_YEAR,jsonObject.getString(Keys.VEHICLE_YEAR));
                            editor.commit();*/
                            moveToNextActivity();

                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.RESISTER);
            progressDialog.show();
        } else {
            Toast.makeText(RegistrationScreen.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void moveToNextActivity() {
        InputMethodManager im = (InputMethodManager) this.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(this.getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        Intent intent = new Intent(RegistrationScreen.this, MainActivity.class);
        startActivity(intent);
        finish();
    }



}
