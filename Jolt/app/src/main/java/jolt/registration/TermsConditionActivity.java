package jolt.registration;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.jolt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import adapters.BusinessCheckListAdapter;
import question.activities.BlogWebViewactivity;
import question.activities.BusinessCheckList;
import utils.ConnectionDetector;
import utils.DataParser;
import utils.INetworkResponse;
import utils.ServiceUrls;
import utils.WebServiceCall;

public class TermsConditionActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private WebView webView;
    private ConnectionDetector connectionDetector;
    private String description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);
        connectionDetector=new ConnectionDetector(TermsConditionActivity.this);
        setupToolbar();
        initializer();
        getTermsAndCondition();
    }

    private void initializer() {
        webView=(WebView)findViewById(R.id.webView);
    }


    // setting all the functionality of toolbar
    private void setUpToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.terms_condition));

        if (toolbar == null) return;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.leftarrow);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.terms_condition));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up
        return false;
    }

    private void setWebView() {
        progressDialog = new ProgressDialog(TermsConditionActivity.this, R.style.MyTheme);
        progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(false);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL("", description, "text/html", "UTF-8", "");
        webView.setWebViewClient(new AppWebViewClients());
    }

    public class AppWebViewClients extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            progressDialog.dismiss();
        }
    }

    private void getTermsAndCondition() {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("id","1");
        } catch (JSONException e) {

        }
        getData(jsonObject);
    }

    private void getData(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(TermsConditionActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            WebServiceCall webServiceCall = new WebServiceCall(TermsConditionActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        if (!response.equals("")) {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean success = jsonObject.getBoolean("success");
                            if (success) {
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                description=jsonObject1.getString("description");
                                setWebView();
                            }
                        } else {
                            Toast.makeText(TermsConditionActivity.this, "Something went wrong.Try Again.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onError(String error) {

                    progressDialog.dismiss();
                    Toast.makeText(TermsConditionActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.TERMS_AND_CONDITIONS_URL);
        } else {
            Toast.makeText(TermsConditionActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

}
