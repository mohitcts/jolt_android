package jolt.registration;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.jolt.AssesmentActivity;
import com.jolt.MainActivity;
import com.jolt.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import fcm.MyFcmTokenModel;
import question.activities.DecisiveActivity;
import utils.AppConstants;
import utils.ConnectionDetector;
import utils.INetworkResponse;
import utils.Keys;
import utils.ServiceUrls;
import utils.Utils;
import utils.WebServiceCall;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    TextView textViewRegistration;
    Button buttonSignIn,buttonCreateAccount;
    EditText editTextEmail, editTextPassword;
    CheckBox cbTermsCondition;
    TextView termsConditionText,textViewForgotPasword;
    ImageView fbCustomImageLogin, btnSignIn;
    CallbackManager callbackManager;
    private ConnectionDetector connectionDetector;
    LoginButton login;
    private static final int RC_SIGN_IN = 007;
    private GoogleApiClient mGoogleApiClient;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private String fcmToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        connectionDetector = new ConnectionDetector(this);

        setupToolbar();
        initFields();
        getFcmKey();
        googlePlusAPI();


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void getFcmKey() {
        MyFcmTokenModel myFcmTokenModel=new MyFcmTokenModel(getApplicationContext());
        fcmToken=myFcmTokenModel.getToken();
        Log.e("token",fcmToken.toString());
        //Toast.makeText(this, ""+token.toString(), Toast.LENGTH_SHORT).show();
    }
    // setting up toolbar
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Sign In");
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ab.setHomeAsUpIndicator(R.mipmap.leftarrow);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up
        return false;
    }


    // Views calling
    private void initFields() {
        callbackManager = CallbackManager.Factory.create();
        //textViewRegistration = (TextView) findViewById(R.id.textViewRegistration);
        buttonSignIn = (Button) findViewById(R.id.buttonSignIn);
        buttonCreateAccount = (Button) findViewById(R.id.buttonCreateAccount);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
       // textViewRegistration.setOnClickListener(this);
        buttonSignIn.setOnClickListener(this);
        buttonCreateAccount.setOnClickListener(this);
        cbTermsCondition = (CheckBox) findViewById(R.id.CBTermsConditions);
        termsConditionText = (TextView) findViewById(R.id.termsConditionText);
        textViewForgotPasword = (TextView) findViewById(R.id.textViewForgotPasword);
        termsConditionText.setOnClickListener(this);
        textViewForgotPasword.setOnClickListener(this);

        login = (LoginButton) findViewById(R.id.login_button);
        fbCustomImageLogin = (ImageView) findViewById(R.id.fbCustomImageLogin);
        fbCustomImageLogin.setOnClickListener(this);

    }

    @Override
    public void onStop() {
        super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            mGoogleApiClient.clearDefaultAccountAndReconnect();
            Log.d("deneme", "onStop");
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();
    }

    // Google plus login method
    public void googlePlusAPI() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestScopes(new Scope(Scopes.PLUS_ME))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        btnSignIn = (ImageView) findViewById(R.id.btn_sign_in_google);
        btnSignIn.setOnClickListener(this);
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // updateUI(false);
                        Log.e(this.getClass().getName(), "BACKPRESSGPLUS" + status);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(this.getClass().getName(), "handleSignInResult:" + result.isSuccess());
        int statusCode = result.getStatus().getStatusCode();
        Log.d(this.getClass().getName(), "GOOGLE_PLUST" + statusCode);
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(this.getClass().getName(), "display name: " + acct.getDisplayName());

            String personName = acct.getDisplayName();

            String firstname = null, lastname = null;
            String fullname = acct.getDisplayName();
            String[] parts = fullname.split("\\s+");
            Log.d("Length-->", "" + parts.length);
            if (parts.length == 2) {
                firstname = parts[0];
                lastname = parts[1];
                Log.d("First-->", "" + firstname);
                Log.d("Last-->", "" + lastname);
            }

            String personPhotoUrl = null;
            Uri PhotoUrl = acct.getPhotoUrl();
            if (PhotoUrl != null) {
                personPhotoUrl = acct.getPhotoUrl().toString();
                // ImageViewProfile= PhotoUrl;
            }

            // LoginBy="Gmail_Login";
            String emailGooglePlus = acct.getEmail();
            if (emailGooglePlus != null) {
                JSONObject jsonObject=new JSONObject();
                try {
                    jsonObject.put(ServiceUrls.PARAMS.EMAIL, emailGooglePlus);
                    jsonObject.put(ServiceUrls.PARAMS.PASSWORD, "");
                    jsonObject.put("fcm_key", fcmToken);
                    jsonObject.put("name", fullname);
                    jsonObject.put("device", "Android");
                    jsonObject.put("registration_type", "social");
                }catch (JSONException e){

                }
                getRegistered(jsonObject);
            }

            Log.e(this.getClass().getName(), "Name: " + personName + ", email: " + emailGooglePlus
                    + ", Image: " + personPhotoUrl);
            //txtName.setText(personName);
            //txtEmail.setText(email);
            //Glide.with(getApplicationContext()).load(personPhotoUrl).thumbnail(0.5f).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL)
            //      .into(imgProfilePic);
            //updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            //updateUI(false);
        }
    }

    private void loginWithFacebook() {
        if (connectionDetector.isConnectingToInternet()) {
            LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "user_friends", "email"));
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

                public void onSuccess(LoginResult result) {
                    GraphRequest request = GraphRequest.newMeRequest(
                            AccessToken.getCurrentAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {
                                    try {
                                        Log.e("json", String.valueOf(object));
                                        String fb_id=object.getString("id").toString().trim();
                                        String first_name = object.getString("first_name").toString().trim();
                                        String last_name = object.getString("last_name").toString().trim();
                                        String email = object.getString("email").toString().trim();
                                        //String profile_pic= object.getJSONObject("picture").getJSONObject("data").getString("url");
                                        //Toast.makeText(SignUpActivity.this, ""+first_name+","+email, Toast.LENGTH_SHORT).show();
                                        JSONObject jsonObject=new JSONObject();

                                        jsonObject.put(ServiceUrls.PARAMS.EMAIL, email);
                                        jsonObject.put(ServiceUrls.PARAMS.PASSWORD, "");
                                        jsonObject.put("fcm_key", fcmToken);
                                        jsonObject.put("name", first_name+" "+last_name);
                                        jsonObject.put("device", "Android");
                                        jsonObject.put("registration_type", "social");
                                        getRegistered(jsonObject);


                                        /*jsonObject.put("fname",first_name);
                                        jsonObject.put("lname",last_name);
                                        jsonObject.put("fb_id",fb_id);
                                        jsonObject.put("email",email);*/
                                       // registerUser(jsonObject);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "first_name,last_name,email,picture");
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                public void onError(FacebookException error) {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

                public void onCancel() {
                    // Toast.makeText(getApplicationContext(), "cancel", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(LoginActivity.this, "Network not Available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonCreateAccount:

                Intent i = new Intent(this, RegistrationScreen.class);
                startActivity(i);

                break;
            case R.id.buttonSignIn:
                getFcmKey();
                if (cbTermsCondition.isChecked()) {
                    // Intent mainActivity = new Intent(this, MainActivity.class);

                    String email=editTextEmail.getText().toString();
                    String password=editTextPassword.getText().toString();
                    if (email.length() <= 0) {
                        editTextEmail.setHintTextColor(Color.RED);
                        editTextEmail.setError("Please Enter Your Email.");
                    } else if (!Utils.isValidEmail(email)) {
                        editTextEmail.setText("");
                        editTextEmail.setHintTextColor(Color.RED);
                        editTextEmail.setError("Please enter a valid email.");
                        Toast.makeText(LoginActivity.this, "PLEASE ENTER A VALID EMAIL", Toast.LENGTH_SHORT).show();
                    }else if(password.length()==0){
                        editTextPassword.setError("Please Enter Password");
                        editTextPassword.requestFocus();
                    }else {

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put(ServiceUrls.PARAMS.EMAIL, editTextEmail.getText().toString());
                            jsonObject.put(ServiceUrls.PARAMS.PASSWORD, editTextPassword.getText().toString());
                            jsonObject.put("fcm_key", fcmToken);
                            jsonObject.put("device", "Android");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(this.getClass().getName(), "JSON_OBJECT" + jsonObject);
                        getLogin(jsonObject);
                    }

                } else {
                    Toast.makeText(this, "Please select terms and conditions", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.termsConditionText:
                startActivity(new Intent(this, TermsConditionActivity.class));
                break;
            case R.id.fbCustomImageLogin:
                loginWithFacebook();
                break;
            case R.id.btn_sign_in_google:
                signIn();
                break;
            case R.id.textViewForgotPasword:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                break;
        }
    }


    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        revokeAccess();
        super.onBackPressed();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(this.getClass().getName(), "onConnectionFailed:" + connectionResult);

    }


    private void getLogin(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(LoginActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        Log.d(this.getClass().getName(),"LOGIN_RESPONSE" + response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        String message = jsonObjectResponse.getString("message");
                        if (success) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            JSONObject jsonObject = jsonObjectResponse.getJSONObject(Keys.DATA);
                            SharedPreferences.Editor editor = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, MODE_PRIVATE).edit();
                            editor.putString(Keys.EMAIL, jsonObject.getString(Keys.EMAIL));
                            editor.putString(Keys.NAME, jsonObject.getString(Keys.NAME));
                            editor.putString(Keys.ID,jsonObject.getString(Keys.ID));
                           // editor.putString(Keys.FCM_KEY,jsonObject.getString(Keys.FCM_KEY));

                            editor.commit();
                            moveToNextActivity();

                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.LOGIN_URL);
            progressDialog.show();
        } else {
            Toast.makeText(LoginActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void getRegistered(JSONObject jsonObject) {
        if (connectionDetector.isConnectingToInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this, R.style.MyTheme);
            progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            WebServiceCall webServiceCall = new WebServiceCall(LoginActivity.this, new INetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        Log.d(this.getClass().getName(),"LOGIN_RESPONSE" + response);
                        boolean success = jsonObjectResponse.getBoolean("success");
                        String message = jsonObjectResponse.getString("message");
                        if (success) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            JSONObject jsonObject = jsonObjectResponse.getJSONObject(Keys.DATA);
                            SharedPreferences.Editor editor = getSharedPreferences(AppConstants.USER_LOGIN_PREFERENCES, MODE_PRIVATE).edit();
                            editor.putString(Keys.EMAIL, jsonObject.getString(Keys.EMAIL));
                            editor.putString(Keys.NAME, jsonObject.getString(Keys.NAME));
                            editor.putString(Keys.ID,jsonObject.getString(Keys.ID));
                            // editor.putString(Keys.FCM_KEY,jsonObject.getString(Keys.FCM_KEY));

                            editor.commit();
                            moveToNextActivity();

                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onError(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
            webServiceCall.execute(jsonObject, ServiceUrls.RESISTER);
            progressDialog.show();
        } else {
            Toast.makeText(LoginActivity.this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void moveToNextActivity() {
        InputMethodManager im = (InputMethodManager) this.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(this.getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Login Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }
}
