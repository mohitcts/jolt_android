package utils;

import android.net.Uri;

/**
 * Created by infoicon on 12/10/15.
 */
public class AppConstants {
    public static final String USER_LOGIN_PREFERENCES = "_user" ;
    public static  Uri uriDoc=Uri.parse("");
    public static boolean isFirstTime=true;


    public static final String NOTIFICATION_PREFERENCE = "_notification" ;

    public static boolean isFromNotification;
    public static boolean isBusinessChecklist;
    public static boolean isMinibusinessPlan;
    public static boolean isCareerPlan;
    public static boolean isBusinessChecklistQuarter;
    public static boolean isNewEmployee;
    public static boolean isEmployee;
    public static boolean isEmployment;
    public static boolean isLeader;
    public static boolean isMainActivityPageOpen;
    public static boolean isSettingPageOpen;
    public static boolean isServiceConfirmed;
    public static boolean isServiceDeclined;
    public static boolean isTowConfirmed;
    public static boolean isTowCompleted;
    public static boolean isFromtow;
    public static boolean isFromService;
    public static boolean isDone=true;
}
