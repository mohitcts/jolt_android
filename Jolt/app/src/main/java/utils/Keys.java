package utils;

import android.os.Environment;

import java.text.SimpleDateFormat;

/**
 * Created by android1 on 2/12/16.
 */

public class Keys {

    public static final String DATA = "data";
    public static final String BUSINESS_CHECKLIST = "business_checklist";
    public static final String MINI_BUSINESS_PLAN = "mini_business_plan";
    public static final String CAREER_PLAN = "career_plan";
    public static final String BUSINESS_CHECKLIST_QUARTER = "business_checklist_quarter";
    public static final String NEW_EMPLOYEE = "new_employee";
    public static final String EMPLOYEE = "employee";
    public static final String EMPLOYMENT = "employment";
    public static final String LEADER = "leader";

    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String STREET = "street";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String FCM_KEY = "fcm_key";
    public static final String PHONE = "phone";
    public static final String PASSWORD = "password";
    public static final String ID = "id";
    public static final String USER_ID = "userid";
    public static final String MOBILE= "mobile";
    public static final String GRADUATION ="graduation";
    public static final String AGE ="age_in_year";
    public static final String PHOTO ="photo";
    public static final String USERID= "user_id";




    public static final String PATH =  Environment.getExternalStorageDirectory().getAbsolutePath() + "/ProjectTemplate";
    public static  int count=1;
    public static SimpleDateFormat SDF = new SimpleDateFormat("ddMMyyyy-hhmmss");

   /** share preferences related key  */

    public static final String PREF_FCM_TOKEN="fcm_token";
    public static final String FCM_TOKEN="token";
    public static final String PREF_USER="user_info";
    public static final String ISLOGIN="isLogin";
    public static final String USERNAME="userName";



    public static final String NOTIFICATION_TYPE="notificaton_type";
    public static final String SERVICE_CONFIRMED="service_confirm";
    public static final String TOW_CONFIRMED="tow_confirm";
    public static final String TOW_COMPLETED="tow_completed";
    public static final String SERVICE_DECLINED="service_declined";

}

