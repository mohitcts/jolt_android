package utils;

public interface INetworkResponse {

    public void onSuccess(String response);
    public void onError(String error);
}
