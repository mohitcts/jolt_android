package utils;

/**
 * Created by taran on 3/3/2017.
 */

public class ServiceUrls {

    public static final String BASE_URL ="http://192.225.175.100/joltapi/";
    public static final String HOST_URL="emott/user/";
    public static final String HOST_URL2="joltapi/appusers/";

    public static final String RESISTER=BASE_URL+"appusers/registration";
    public static final String LOGIN_URL =BASE_URL+"appusers/login";
    public static final String QUESTIONS_URL =BASE_URL+"assessments/question";
    public static final String RATE_URL =BASE_URL+"assessments/rate";
    public static final String ASSESMENT_PERSONALITY_URL =BASE_URL+"assessments/personalities";
    public static final String CAREER_PLANNING_URL ="https://api-v2.themuse.com/posts?page=";
    public static final String ALL_BUSSINESS_CHECKLIST_URL =BASE_URL+"businessplans/allchecklist";
    public static final String ALL_CHECKLIST_URL =BASE_URL+"career/allchecklist";
    public static final String ADD_BUSSINESS_CHECKLIST_URL =BASE_URL+"businessplans/addchecklist";
    public static final String ADD_CHECKLIST_URL =BASE_URL+"career/addchecklist";
    public static final String GET_COACH_CATEGORY_URL =BASE_URL+"appusers/coach_category";
    public static final String REGISTER_COACH_URL =BASE_URL+"appusers/coach_register";
    public static final String JOB_BOARD_URL ="https://api-v2.themuse.com/jobs?page=";
    public static final String GET_USER_PROFILE =BASE_URL +"appusers/getprofile";
    public static final String EDIT_PROFILE_URL =BASE_URL +"appusers/editprofile";
    public static final String MINI_BUSSINESS_PLAN_URL =BASE_URL+"businessplans/addplan";
    public static final String CAREER_PLANNER_URL =BASE_URL+"career/addplan";
    public static final String FIND_A_RECRUITER_URL =BASE_URL+"recruiters/recruitlist";
    public static final String TERMS_AND_CONDITIONS_URL =BASE_URL+"cms/terms";
    public static final String FORGOT_PASSWORD_URl =BASE_URL+"appusers/forgetpassword";
    public static final String SUBMIT_PAYMENT_DETAILS =BASE_URL+"planpayment/adddetail";
    public static final String GET_PAYMENT_DETAILS =BASE_URL+"planpayment/getstatus";
    public static final String SET_REMINDERS =BASE_URL+"appusers/updatetime";
    public static final String FEEDBACK_URL =BASE_URL+"feedback/form";


    public class PARAMS{
        public static final String USER_ID ="user_id";
        public static final String NAME="name";
        public static final String USER_NAME="user_name";
        public static final String EMAIL="email";
        public static final String STATUS="status";
        public static final String CREATED="created";
        public static final String UPDATED="updated";
        public static final String PASSWORD="password";
        public static final String PHONE="phone";
        public static final String DOB="dob";
        public static final String DATA="data";
    }
}
