package utils;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by info12 on 15/12/15.
 */
public class WebServiceGet {
    Context context;
    private String errorMsg;
    INetworkResponse iNetworkResponse;
    private String responseString="";

    public WebServiceGet(Context context, INetworkResponse iNetworkResponse) {
        this.context = context;
        this.iNetworkResponse = iNetworkResponse;
    }

    public void execute(String url) {
        CatalogClient call = new CatalogClient();
        call.execute(url);
    }


    public class CatalogClient extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {

            HttpURLConnection urlConnection = null;
            JSONObject response = new JSONObject();

            try {
                URL url = new URL(params[0]);
              // urlConnection.setConnectTimeout(10000);
                /* urlConnection.setReadTimeout(10000);*/
                //urlConnection.setDoOutput(true);
                urlConnection = (HttpURLConnection) url.openConnection();
                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpStatus.SC_OK) {
                    responseString = readStream(urlConnection.getInputStream());
                } else {

                }

            } catch (UnknownHostException e) {
                errorMsg="Network is Not Available.";
                e.printStackTrace();
            } catch (SocketTimeoutException e){
                errorMsg="Network is Too Slow.";
               e.printStackTrace();
            } catch (IOException e){
                errorMsg="Network is Too Slow.";
                e.printStackTrace();
            }catch (Exception e){
                errorMsg="Something went wrong. Try Again.";
                e.printStackTrace();
            }  finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                iNetworkResponse.onSuccess(responseString);
            } else {
                iNetworkResponse.onError(errorMsg);
            }

        }

        private String readStream(InputStream in) {
            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }
}
