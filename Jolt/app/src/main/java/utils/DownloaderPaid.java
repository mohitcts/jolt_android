package utils;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.jolt.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import question.activities.MiniPDfViewActivity;
import question.activities.PaidAssessmentActivity;

/**
 * Created by rishabh on 5/17/2017.
 */

public class DownloaderPaid  extends AsyncTask<Void, Void, Void> {
    private String fileURL;
    private String type;
    private File directory;
    private Context context;
    private ProgressDialog progressDialog;
    private String extension;

    public DownloaderPaid(Context activity, String fileURL, File directory, String extension, String type) {
        this.fileURL = fileURL;
        this.directory = directory;
        this.context = activity;
        this.extension = extension;
        this.type = type;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context, R.style.MyTheme);
        progressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            FileOutputStream f = new FileOutputStream(directory);
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressDialog.dismiss();

        Toast.makeText(context, "Downloaded and saved in your device inside JOLT folder.", Toast.LENGTH_SHORT).show();

        /*PackageManager packageManager = context.getPackageManager();

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(directory);
        intent.setDataAndType(AppConstants.uriDoc, "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        try {
            context.startActivity(intent);
        }
        catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No Pdf Reader found in your device. Please install.", Toast.LENGTH_SHORT).show();
        }*/



        /*Bundle bundle = new Bundle();
        bundle.putString("url", fileURL);
        bundle.putString("type", type);
        Intent intent = new Intent(context, MiniPDfViewActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);*/

        //   progressDialog.dismiss();

        /*String fileName = fileURL.substring(fileURL.lastIndexOf('/') + 1);
        File file = new File(Environment.getExternalStorageDirectory() + "/DesignFormFurnishing/" + fileName);*/
        /*if(extension.equals("pdf")) {
            //Toast.makeText(context, R.string.pdfDownloaded, Toast.LENGTH_SHORT).show();
            PackageManager packageManager = context.getPackageManager();
            Intent testIntent = new Intent(Intent.ACTION_VIEW);
            testIntent.setType("application/pdf");
            List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(directory);
            intent.setDataAndType(uri, "application/pdf");
            context.startActivity(intent);
        }*/
    }
}