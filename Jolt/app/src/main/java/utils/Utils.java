package utils;

import android.text.TextUtils;

/**
 * Created by taran on 3/8/2017.
 */

public class Utils {


    public final static boolean isValidEmail(CharSequence target) {
        boolean a;
        a = !TextUtils.isEmpty(target)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                .matches();
        return a;
    }

}
