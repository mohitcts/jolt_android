package utils;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by infoicon on 12/10/15.
 */
public class MultipartWebServiceCall {

    Context context;
    private String errorMsg;
    INetworkResponse iNetworkResponse;
    private MultipartEntityBuilder multipartEntityBuilder;


    public MultipartWebServiceCall(Context context, INetworkResponse iNetworkResponse) {
        this.context = context;
        this.iNetworkResponse = iNetworkResponse;
        this.multipartEntityBuilder=multipartEntityBuilder;
    }

    public void execute(MultipartEntityBuilder multipartEntityBuilder,String url) {
        Call call = new Call(multipartEntityBuilder);
        call.execute(url);
    }


    private class Call extends AsyncTask<String, Void, Boolean> {

        private static final String USER_AGENT = "Mozilla/5.0";
        private String image;
        JSONObject jsonObject;
        private String responseString;
        private MultipartEntityBuilder multipartEntityBuilder;

        public Call(MultipartEntityBuilder multipartEntityBuilder) {
            this.multipartEntityBuilder=multipartEntityBuilder;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Boolean doInBackground(String... params) {
            {
                String boundary = "Infoicon" + System.currentTimeMillis();
                HttpEntity entity = multipartEntityBuilder.build();

                URL url = null;

                try {
                    url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(50000);
                    urlConnection.setReadTimeout(50000);
                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestMethod("POST");
                    urlConnection.addRequestProperty("Content-length", entity.getContentLength()+"");
                    urlConnection.addRequestProperty(entity.getContentType().getName(), entity.getContentType().getValue());

                    OutputStream os = urlConnection.getOutputStream();
                    entity.writeTo(urlConnection.getOutputStream());
                    os.close();
                    urlConnection.connect();
                    InputStream inputStream = urlConnection.getInputStream();

                    BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
                    String s="";
                    StringBuilder stringBuilder = new StringBuilder("");
                    while ((s=bufferedReader.readLine())!=null)
                    {
                        stringBuilder.append(s);
                    }
                    responseString = stringBuilder.toString();
                    return true;
                }
                catch (UnknownHostException e) {
                    e.printStackTrace();
                    errorMsg = "Network Not Available!";
                } catch (SocketTimeoutException e) {
                    e.printStackTrace();
                    errorMsg = "Network is too Slow.";
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                    errorMsg = "Server not Responding";
                }
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if (result) {
                iNetworkResponse.onSuccess(responseString);
            } else {
                iNetworkResponse.onError(errorMsg);
            }


        }
    }
}
