package utils;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by MaNoJ SiNgH RaWaL on 22-May-15.
 */
public class WebServiceCall {

    Context context;
    private String errorMsg;
    INetworkResponse iNetworkResponse;

    public WebServiceCall(Context context, INetworkResponse iNetworkResponse) {
        this.context = context;
        this.iNetworkResponse = iNetworkResponse;
    }

    public void execute(JSONObject jsonObject, String url) {
        Call call = new Call(jsonObject);
        call.execute(url);
    }


    private class Call extends AsyncTask<String, Void, Boolean> {

        public Call(){

        }
        private static final String USER_AGENT = "Mozilla/5.0";
        JSONObject jsonObject;
        private String responseString;
      //  private ProgressDialog progressDialog;

        public Call(JSONObject jsonObject) {
            this.jsonObject = jsonObject;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
         //   progressDialog = ProgressDialog.show(context, "Please Wait", "Searching...");

        }

        @Override
        protected Boolean doInBackground(String... params) {

            try {
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(50000);
                urlConnection.setReadTimeout(50000);
                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Users-Agent", USER_AGENT);
                urlConnection.setRequestProperty("Accept-Encoding", "identity");
                urlConnection.setRequestProperty("content-type", "application/json");

                DataOutputStream dataOutputStream = new DataOutputStream(urlConnection.getOutputStream());
                dataOutputStream.writeBytes(jsonObject.toString());
                dataOutputStream.flush();
                dataOutputStream.close();


                InputStream inputStream = urlConnection.getInputStream();

                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
                String s="";
                StringBuilder stringBuilder = new StringBuilder("");
                while ((s=bufferedReader.readLine())!=null)
                {
                    stringBuilder.append(s);
                }
                responseString = stringBuilder.toString();
                return true;
            } catch (UnknownHostException e) {
                e.printStackTrace();
                errorMsg = "Network Not Available!";
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                errorMsg = "Network is too Slow!";
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                errorMsg = "Server not Responding. Please try again.";
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
                errorMsg = "Something went wrong. Please try again.";
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

           /* if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();*/
            if (result) {
                iNetworkResponse.onSuccess(responseString);
            } else {
                iNetworkResponse.onError(errorMsg);
            }
        }


    }
    public void cancel() {
        Call call = new Call();
        if (call != null && call.getStatus() != AsyncTask.Status.FINISHED)
            call.cancel(true);

        //  call.cancel(true);
    }
}
