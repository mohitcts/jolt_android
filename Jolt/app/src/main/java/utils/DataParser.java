package utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import projections.ItemBussinessChecklist;
import projections.ItemCareerAdvice;
import projections.ItemCareerPlaning;
import projections.ItemCoachcategories;
import projections.ItemDecisiveQuestions;
import projections.ItemFindARecruiter;

/**
 * Created by rishabh on 3/15/2017.
 */

public class DataParser {

    public ArrayList<ItemDecisiveQuestions> parseDecesiveQuestions(JSONArray jsonArray) {
        ArrayList<ItemDecisiveQuestions> decisiveQuestionArrayList = new ArrayList<ItemDecisiveQuestions>();
        ItemDecisiveQuestions itemDecisiveQuestions;
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                itemDecisiveQuestions = new ItemDecisiveQuestions();
                itemDecisiveQuestions.setId(jsonObject.getString("id"));
                itemDecisiveQuestions.setCategory(jsonObject.getString("category"));
                itemDecisiveQuestions.setQuestion(jsonObject.getString("question1"));
                itemDecisiveQuestions.setQuestion2(jsonObject.getString("question2"));
                decisiveQuestionArrayList.add(itemDecisiveQuestions);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return decisiveQuestionArrayList;
    }

    public ArrayList<ItemCareerPlaning> parseCareerPlanningData(JSONArray jsonArray) {
        ArrayList<ItemCareerPlaning> careerPlanningArrayList = new ArrayList<ItemCareerPlaning>();
        ItemCareerPlaning itemCareerPlaning;
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                itemCareerPlaning = new ItemCareerPlaning();
                itemCareerPlaning.setExcerpt(jsonObject.getString("excerpt"));
                itemCareerPlaning.setName(jsonObject.getString("name"));
                itemCareerPlaning.setTags(jsonObject.getJSONArray("tags"));
                itemCareerPlaning.setRefs(jsonObject.getJSONObject("refs"));
                careerPlanningArrayList.add(itemCareerPlaning);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return careerPlanningArrayList;
    }


    public ArrayList<ItemCareerAdvice> parseJoBBoardData(JSONArray jsonArray) {
        ArrayList<ItemCareerAdvice> careerAdviceArrayList = new ArrayList<ItemCareerAdvice>();
        ItemCareerAdvice itemCareerPlaning;
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                itemCareerPlaning = new ItemCareerAdvice();
              //  itemCareerPlaning.setExcerpt(jsonObject.getString("excerpt"));
                itemCareerPlaning.setName(jsonObject.getString("name"));
                itemCareerPlaning.setLocations(jsonObject.getJSONArray("locations"));
                itemCareerPlaning.setCompany(jsonObject.getJSONObject("company"));
                itemCareerPlaning.setRefs(jsonObject.getJSONObject("refs"));
                careerAdviceArrayList.add(itemCareerPlaning);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return careerAdviceArrayList;
    }



    public ArrayList<ItemCoachcategories> parseCoachCategoryData(JSONArray jsonArray) {
        ArrayList<ItemCoachcategories> coachCategoryArrayList = new ArrayList<ItemCoachcategories>();
        ItemCoachcategories itemCoachcategories;
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                itemCoachcategories = new ItemCoachcategories();
                itemCoachcategories.setCategory_name(jsonObject.getString("category_name"));
                itemCoachcategories.setCatid(jsonObject.getString("catid"));
                coachCategoryArrayList.add(itemCoachcategories);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return coachCategoryArrayList;
    }

    public ArrayList<ItemFindARecruiter> parseFindARecruiterData(JSONArray jsonArray) {
        ArrayList<ItemFindARecruiter> findARecruiterArrayList = new ArrayList<ItemFindARecruiter>();
        ItemFindARecruiter itemFindARecruiter;
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                itemFindARecruiter = new ItemFindARecruiter();
                itemFindARecruiter.setAddress(jsonObject.getString("address"));
                itemFindARecruiter.setCompany(jsonObject.getString("company"));
                itemFindARecruiter.setFax(jsonObject.getString("fax"));
                itemFindARecruiter.setId(jsonObject.getString("id"));
                itemFindARecruiter.setService_offered(jsonObject.getString("service_offered"));
                itemFindARecruiter.setTelephone(jsonObject.getString("telephone"));
                itemFindARecruiter.setWebsite(jsonObject.getString("website"));
                findARecruiterArrayList.add(itemFindARecruiter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return findARecruiterArrayList;
    }

    public ArrayList<ItemBussinessChecklist> parseBussinessCheckListData(JSONArray jsonArray) {
        ArrayList<ItemBussinessChecklist> bussinessCheckListArrayList = new ArrayList<ItemBussinessChecklist>();
        ItemBussinessChecklist itemBussinessChecklist;
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                itemBussinessChecklist = new ItemBussinessChecklist();
                itemBussinessChecklist.setChecklist_name(jsonObject.getString("checklist_name"));
                itemBussinessChecklist.setQuestion_id(jsonObject.getString("checklist_id"));
                itemBussinessChecklist.setCheck_status(jsonObject.getString("check_status"));
                bussinessCheckListArrayList.add(itemBussinessChecklist);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return bussinessCheckListArrayList;
    }
}
