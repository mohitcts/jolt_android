package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.jolt.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import projections.ItemBussinessChecklist;
import projections.ItemCareerPlaning;
import utils.AppConstants;

/**
 * Created by rishabh on 4/24/2017.
 */

public class BusinessCheckListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ItemBussinessChecklist> bussinessChecklistArrayList;
    public ArrayList<Integer> checkboxListStatus;
    public ArrayList<String> checkedArrayList;

    public BusinessCheckListAdapter(Context context, ArrayList<ItemBussinessChecklist> bussinessChecklistArrayList, ArrayList<Integer> checkboxListStatus,ArrayList<String> checkedArrayList) {
        this.context = context;
        this.bussinessChecklistArrayList = bussinessChecklistArrayList;
        this.checkboxListStatus = checkboxListStatus;
        this.checkedArrayList = checkedArrayList;
    }

    public ArrayList<String> getCheckedArrayList() {
        return checkedArrayList;
    }

    @Override
    public int getCount() {
        return bussinessChecklistArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return bussinessChecklistArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        View rowView = view;
        if (rowView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_business_checklist, viewGroup,
                    false);
            viewHolder.checkBox = (CheckBox) rowView.findViewById(R.id.checkBox);
            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        String checkListtext = bussinessChecklistArrayList.get(position).getChecklist_name();
        viewHolder.checkBox.setText(checkListtext);

        String check_status = bussinessChecklistArrayList.get(position).getCheck_status();


        if (checkboxListStatus.contains(position)) {
            viewHolder.checkBox.setChecked(true);
        } else {
            viewHolder.checkBox.setChecked(false);
        }


        viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkboxListStatus.contains(position)) {
                    for (int i = 0; i < checkboxListStatus.size(); i++) {
                        if (checkboxListStatus.get(i) == position) {
                            checkboxListStatus.remove(i);
                            checkedArrayList.remove(i);
                        }
                    }
                } else {
                    checkboxListStatus.add(position);
                    checkedArrayList.add(bussinessChecklistArrayList.get(position).getQuestion_id());
                }

                /*if(checkedOptionPriceArrayList.size()>0){
                    VehicleInformationFragment.textViewDone.setVisibility(View.VISIBLE);
                }else {
                    VehicleInformationFragment.textViewDone.setVisibility(View.INVISIBLE);
                }*/
            }
        });
        return rowView;
    }

    public class ViewHolder {
        public CheckBox checkBox;
    }
}
