package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jolt.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import projections.ItemCareerPlaning;

/**
 * Created by rishabh on 3/31/2017.
 */

public class CareerPlanningAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ItemCareerPlaning> careerPlaningArrayList;
    private String category_id;
    private String image_url;

    public CareerPlanningAdapter(Context context, ArrayList<ItemCareerPlaning> careerPlaningArrayList) {
        this.context = context;
        this.careerPlaningArrayList = careerPlaningArrayList;
    }

    @Override
    public int getCount() {
        return careerPlaningArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return careerPlaningArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        View rowView = view;
        if (rowView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_career_planning, viewGroup,
                    false);
            viewHolder.textViewName = (TextView) rowView.findViewById(R.id.textViewName);
            viewHolder.textViewDescription = (TextView) rowView.findViewById(R.id.textViewDescription);
            viewHolder.imageViewCategory = (ImageView) rowView.findViewById(R.id.imageViewCategory);
            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        String name=careerPlaningArrayList.get(position).getName();
        String description=careerPlaningArrayList.get(position).getExcerpt();
        viewHolder.textViewName.setText(name);
        viewHolder.textViewDescription.setText(description);

        JSONObject jsonObjectRefs=careerPlaningArrayList.get(position).getRefs();
        try {
            image_url=jsonObjectRefs.getString("primary_image");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(!image_url.equals("") && !image_url.equals("null")){
            viewHolder.imageViewCategory.setVisibility(View.VISIBLE);
            Picasso.with(context).load(image_url).placeholder(R.drawable.thumbnail).into(viewHolder.imageViewCategory);
        }else {
            viewHolder.imageViewCategory.setVisibility(View.GONE);
        }

        return rowView;
    }

    public class ViewHolder {
        public TextView textViewName,textViewDescription;
        private ImageView imageViewCategory;
    }
}
