package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jolt.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import projections.ItemCareerAdvice;

/**
 * Created by taran on 4/11/2017.
 */

public class JobBoardAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ItemCareerAdvice> careerAdviceArrayList;
    private String category_id;
    private String image_url;
    private String company_name;
    private String locationName;

    public JobBoardAdapter(Context context, ArrayList<ItemCareerAdvice> careerAdviceArrayList){
        this.context= context;
        this.careerAdviceArrayList= careerAdviceArrayList;

    }

    @Override
    public int getCount() {
        return careerAdviceArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return careerAdviceArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        View rowView = convertView;
        if (rowView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_career_advice, parent,
                    false);
            viewHolder.textViewName = (TextView) rowView.findViewById(R.id.textViewName);
            viewHolder.textViewDescription = (TextView) rowView.findViewById(R.id.textViewDescription);
            viewHolder.textViewCompanyName=(TextView)rowView.findViewById(R.id.textViewCompanyName);
            //viewHolder.imageViewCategory = (ImageView) rowView.findViewById(R.id.imageViewCategory);
            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        String name=careerAdviceArrayList.get(position).getName();
       // String description=careerAdviceArrayList.get(position).getExcerpt();
        viewHolder.textViewName.setText(name);
        //

        JSONObject jsonObjectCompanyName = careerAdviceArrayList.get(position).getCompany();
        try {
            company_name= jsonObjectCompanyName.getString("name");

        }catch (JSONException e){
            e.printStackTrace();
        }
        JSONArray jsonArrayLocation = careerAdviceArrayList.get(position).getLocations();
        try {
            JSONObject jsonObject =jsonArrayLocation.getJSONObject(0);
            locationName= jsonObject.getString("name");


        }catch (JSONException e){
            e.printStackTrace();
        }
        viewHolder.textViewCompanyName.setText(company_name);
        viewHolder.textViewDescription.setText(locationName);

        /*JSONObject jsonObjectRefs=careerAdviceArrayList.get(position).getRefs();
        try {
            image_url=jsonObjectRefs.getString("primary_image");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(!image_url.equals("") && !image_url.equals("null")){
            viewHolder.imageViewCategory.setVisibility(View.VISIBLE);
            Picasso.with(context).load(image_url).placeholder(R.drawable.thumbnail).into(viewHolder.imageViewCategory);
        }else {
            viewHolder.imageViewCategory.setVisibility(View.GONE);
        }*/

        return rowView;
    }

    public class ViewHolder{
        public TextView textViewName,textViewDescription,textViewCompanyName;
        //private ImageView imageViewCategory;
    }

}
