package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jolt.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import projections.ItemCareerPlaning;
import projections.ItemFindARecruiter;

/**
 * Created by rishabh on 4/22/2017.
 */

public class FindARecruiterAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ItemFindARecruiter> findARecruiterArrayList;
    private String category_id;
    private String image_url;

    public FindARecruiterAdapter(Context context, ArrayList<ItemFindARecruiter> findARecruiterArrayList) {
        this.context = context;
        this.findARecruiterArrayList = findARecruiterArrayList;
    }

    @Override
    public int getCount() {
        return findARecruiterArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return findARecruiterArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        View rowView = view;
        if (rowView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_find_a_recriter, viewGroup,
                    false);
            viewHolder.textViewServices = (TextView) rowView.findViewById(R.id.textViewServices);
            viewHolder.textViewCompany = (TextView) rowView.findViewById(R.id.textViewCompany);
            viewHolder.textViewAddress = (TextView) rowView.findViewById(R.id.textViewAddress);
            viewHolder.textViewTelephone = (TextView) rowView.findViewById(R.id.textViewTelephone);
            viewHolder.textViewFax = (TextView) rowView.findViewById(R.id.textViewFax);
            viewHolder.textviewWebsite = (TextView) rowView.findViewById(R.id.textviewWebsite);
            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        String service=findARecruiterArrayList.get(position).getService_offered();
        String company=findARecruiterArrayList.get(position).getCompany();
        String address=findARecruiterArrayList.get(position).getAddress();
        String telephone=findARecruiterArrayList.get(position).getTelephone();
        String fax=findARecruiterArrayList.get(position).getFax();
        String website=findARecruiterArrayList.get(position).getWebsite();

        viewHolder.textViewAddress.setText(address);
        viewHolder.textViewCompany.setText(company);
        viewHolder.textViewFax.setText(fax);
        viewHolder.textViewServices.setText(service);
        viewHolder.textViewTelephone.setText(telephone);
        viewHolder.textviewWebsite.setText(website);

        return rowView;
    }

    public class ViewHolder {
        public TextView textViewServices,textViewCompany,textViewAddress,textViewTelephone,textViewFax,textviewWebsite;
    }
}